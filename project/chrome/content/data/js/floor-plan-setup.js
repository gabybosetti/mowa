function SensorsLoadingData(){}
SensorsLoadingData.prototype.loadControlsData = function(){

	appBuilder.loadFloorPlanePreview('floorPlanePrview');
}

try{
	window.appBuilder = new MHAppsBuilder(); 
	appBuilder.startBuildingProcess(function(){
	    appBuilder.initialize(new SensorsLoadingData()); 
        appBuilder.validateImageUrl();
        //appBuilder.viewManager.setControlAsReadOnly();            	
	});
}catch(err){console.log(err.message)}

window.onload = function(){

	//NAVIGATION
	document.querySelector(".next > a").onclick = function(){
		
		appBuilder.loadUriWithApi("floor-plan-pois-management.html");
        //appBuilder.saveCurrentArtifactData();
	}
	document.querySelector("#configureMapUrl").onclick = function(){
		
        //appBuilder.loadLeafletRefInTemporalModelRef();
        appBuilder.loadUriWithApi('UrlSelector.xml','subform');
	}
}