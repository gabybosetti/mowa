# Run this script from your project base dir.

#1 - Complete the following vars:
ANDROID_APP_ID=org.mozilla.fennec;
#ANDROID_APP_ID=org.mozilla.firefox_beta; 
#ANDROID_APP_ID=org.mozilla.firefox;

APP_NAME="MoWA";
INPUT_DIR=".";
FILE="DarwinTourApplication.js";

# 01) copy the file to mobile
adb push $INPUT_DIR/$FILE /sdcard/$FILE

# 02) open the file in browser
adb shell am start -a android.intent.action.VIEW -c android.intent.category.DEFAULT -d file:///mnt/sdcard/$FILE -n $ANDROID_APP_ID/.App
