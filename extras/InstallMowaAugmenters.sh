# Run this script from your project base dir.

#1 - Complete the following vars:
ANDROID_APP_ID=org.mozilla.fennec;
#ANDROID_APP_ID=org.mozilla.firefox_beta; 
#ANDROID_APP_ID=org.mozilla.firefox;

APP_NAME="MoWA";
INPUT_DIR="./augmenters/*";

for file in $INPUT_DIR
do
	echo "File: "+$file;
    adb push $file /sdcard/$file
    adb shell am start -a android.intent.action.VIEW -c android.intent.category.DEFAULT -d file:///mnt/sdcard/$file -n $ANDROID_APP_ID/.App

    echo ""; read -p "Accept and install the augmenter. Press any key to install the next one...";
    adb shell rm /sdcard/$file
done
