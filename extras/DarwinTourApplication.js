//::MoWA::
	//@scriptType: MobileApplication
	//@className: DarwinTourApplication
	//@requiredAugmenters: HtmlObjectAugmenter PoiAugmenter ListAugmenter WalkingLinksAugmenter
	//@namespace: org.dev01	
	//@author: lifia
//::MoWA::

function DarwinTourApplication(name, traversingStrategyName) {

	MobileHypermediaApp.prototype.constructor.call(name, traversingStrategyName);
};
DarwinTourApplication.prototype = new MobileHypermediaApp(
	"Recorrido de Darwin en el Museo de Ciencias Naturales de La Plata",
	"StrictOrderedStrategy"
);
DarwinTourApplication.prototype.initialize = function(){

	/********** RELATED TO AUGMENTERS **********/
	/** Create an instance of DarwinTourApplication. */
	this.setDefaultXpath("//*[@id='section_0' or @id='firstHeading' or @id='headcontent']");

	/********** POINT OF INTERESTS SPECIFICATION **********/
	/** Create a main PoI. */
	var mhpMuseo = this.createMainPoi(
		"Museo", 
		"http://es.wikipedia.org/wiki/Museo_de_La_Plata", 
		new GpsLocation(-34.00000,-57.000000), 
		null, 
		{	image:"http://www.fcnym.unlp.edu.ar/museo/educativa/darwin/imagenes/plano.jpg", 
			description: "Museo de Ciencias Naturales de La Plata"}, 
		null
	);

	/** Create a set of PoI. */
	var mhpMacrauchenia = this.createLeafPoi(
		"Macrauchenia", 
		"http://es.m.qrwp.org/Macrauchenia_patachonica",
		new QRLocation("http://es.m.qrwp.org/Macrauchenia_patachonica"),
		{image:"http://www.fcnym.unlp.edu.ar/museo/educativa/darwin/imagenes/macrauquenia.jpg"},
		{description: {url:"http://www.fcnym.unlp.edu.ar/museo/educativa/darwin/piezas/pieza1.html", xpathExpression: "//*[@id='contenido']"}}
	)
	var mhpGlyptodon = this.createLeafPoi(
		"Glyptodon", 
		"http://es.m.qrwp.org/Glyptodon",
		new QRLocation("http://es.m.qrwp.org/Glyptodon"),
		{image:"http://www.fcnym.unlp.edu.ar/museo/educativa/darwin/imagenes/gliptodontesgr.jpg"},
		{description: {url:"http://www.fcnym.unlp.edu.ar/museo/educativa/darwin/piezas/pieza2.html", xpathExpression: "//*[@id='contenido']"}}
	)
	var mhpToxodon = this.createLeafPoi(
		"Toxodon", 
		"http://es.m.qrwp.org/Toxodon",
		new QRLocation("http://es.m.qrwp.org/Toxodon"),
		{image:"http://www.fcnym.unlp.edu.ar/museo/educativa/darwin/imagenes/toxodon.jpg"},
		{description: {url:"http://www.fcnym.unlp.edu.ar/museo/educativa/darwin/piezas/pieza3.html", xpathExpression: "//*[@id='contenido']"}}
	)
	var mhpMegatherium = this.createLeafPoi(
		"Megatherium", 
		"http://es.m.qrwp.org/Megatherium",
		new QRLocation("http://es.m.qrwp.org/Megatherium"),
		{image:"http://www.fcnym.unlp.edu.ar/museo/educativa/darwin/imagenes/megaterio.jpg"},
		{description: {url:"http://www.fcnym.unlp.edu.ar/museo/educativa/darwin/piezas/pieza4.html", xpathExpression: "//*[@id='contenido']"}}
	)

	/** Add PoIs to the main place */
	mhpMuseo.addContainedPoi(mhpMacrauchenia);
	mhpMuseo.addContainedPoi(mhpGlyptodon);
	mhpMuseo.addContainedPoi(mhpToxodon);
	mhpMuseo.addContainedPoi(mhpMegatherium);

	/********** PHYSICAL SPACE NAVIGATION SPECIFICATION **********/
	/** Add starting PoI and then connect every one. */
	this.addAsStartingPoi([mhpMacrauchenia]);
	this.createWalkingLink([mhpMacrauchenia], [mhpGlyptodon]);
	this.createWalkingLink([mhpGlyptodon], [mhpToxodon]);
	this.createWalkingLink([mhpToxodon], [mhpMegatherium]);

	/********** PHYSICAL SPACE REPRESENTATION SPECIFICATION **********/
	/** Add representations to the space of representation. 
	 Should be noted that representations must be consistent to the 
	 type of space of representation chosen */
	 /** Create an instance of a kind of RespresentationSystem. Eg: BidimensionalPlane. */
	var museumMap = new CustomMap(); 
	museumMap.addMarker(mhpMacrauchenia, new CustomMapLocation(233, 39));
	museumMap.addMarker(mhpGlyptodon, new CustomMapLocation(172.5, 20));
	museumMap.addMarker(mhpToxodon, new CustomMapLocation(80, 93));
	museumMap.addMarker(mhpMegatherium, new CustomMapLocation(222.5, 39));

	mhpMuseo.addSpaceRepresentation(museumMap);
};

/** Create the specific adaptation functions. When a page be loaded 
* one of these functions will be called according to the requested Poi.
* Specific adaptation can call genericAdaptation if wants to use it. */
DarwinTourApplication.prototype.adaptMacrauchenia = function(){	

	this.genericAdaptation();
};
DarwinTourApplication.prototype.adaptGlyptodon = function(){	

	this.genericAdaptation();
};
DarwinTourApplication.prototype.adaptToxodon = function(){	

	this.genericAdaptation();
};
DarwinTourApplication.prototype.adaptMegatherium = function(){	

	this.genericAdaptation();
};

/** Create the generic adaptation function. */
DarwinTourApplication.prototype.genericAdaptation = function(){  

	//Se crea un párrafo con el título de la aplicación
	var titleParagraph = window.content.document.createElement("p");
	titleParagraph.style.fontWeight = 'bold';
	titleParagraph.style.margin = "0";
	titleParagraph.appendChild(window.content.document.createTextNode(this.getName()));
	
	//Se muestra el párrafo mediante un augmenter
	var htmlObjectAugmenter = this.getAugmenterInstance("HtmlObjectAugmenter").execute({
		"elemId": "darwinTitle", 
		"xpathToPrevElem": this.getDefaultXpath(), 
		"htmlObjects":[titleParagraph]
	});

	//Se inicializa y ejecuta un PoiAugmenter para mostrar información sobre el lugar en la adaptación 
	var poiAugmenter = this.getAugmenterInstance("PoiAugmenter");
	poiAugmenter.execute({
		"elemId": "darwinPoiAugmenter", 
		"xpathToPrevElem": "//*[@id='darwinTitle']",
		"poi":this.getCurrentPoi()
	});

	//Se crea la variable para el visitedPoisAugmenter, con el fin de poder 
	//referenciarlo desde el undo(params) del WalkingLinksAugmenter
	var poisListerAugmenter = this.getAugmenterInstance("ListAugmenter");
	
	//Se inicializa y ejecuta un WalkingLinksAugmenter para mostrar una lista de próximos objetos a visitar 
	this.getAugmenterInstance("WalkingLinksAugmenter").execute({	
		"elemId": "darwinWalkingLinks", 
		"xpathToPrevElem": "//*[@id='darwinPoiAugmenter']", 
		"description": "Siguientes objetos: ", 
		"pois": this.getNextWalkingLinks(), 
		"xpathForPathAugmenter": "//*[@id='darwinTitle']", 
		"app": this, 
		"undoAugmentations": function(){
			poiAugmenter.undo({"elemId":"darwinPoiAugmenter"});
			poisListerAugmenter.undo({"elemId":"darwinVisitedPois"});
			poisListerAugmenter.undo({"elemId":"darwinUnvisitedPois"});
		} 
	});
	//TODO: pois should not be a parameter. xpathToPrevElem is twice defined in function's parameters.

	//Se ejecuta el poisListerAugmenter para mostrar primero los objetos ya visitados
	//y luego los que restan por visitar
	poisListerAugmenter.execute({
		"elemId": "darwinVisitedPois", 
		"xpathToPrevElem":"//*[@id='darwinWalkingLinks']",  
		"description":"Objetos visitados: ",
		"pois": this.getVisitedPois()
	});

	poisListerAugmenter.execute({
		"elemId": "darwinUnvisitedPois", 
		"xpathToPrevElem":"//*[@id='darwinVisitedPois']",  
		"description":"Objetos sin visitar: ",
		"pois": this.getUnvisitedPois()
	});
};

