<?php 
	header('Access-Control-Allow-Origin: *');
	/*TODO: security validations... */
	if (isset($_POST['code']) and isset($_POST['filename_in_tool']) and 
		!empty($_POST['code']) and !empty($_POST['filename_in_tool']) and 
		$_POST['code']=='mowa.upload.x.result') {  

		include 'mowa_conn_vars.php'; /* $servername $username  $password  $dbname */

		$conn = new mysqli($servername, $username, $password, $dbname);
		if ($conn->connect_error) { die("Connection failed: " . $conn->connect_error);}

		$sql = "INSERT INTO ExperimentResults (filename_in_tool, file, userAgent, navigatorLanguage) 
				VALUES (?, ?, ?, ?)";
		$stmt = $conn->stmt_init();
		$stmt->prepare($sql);
		$stmt->bind_param("ssss", $filename_in_tool, $file, $userAgent, $navigatorLanguage);

		$filename_in_tool = $_POST['filename_in_tool'];
		$file = $_POST['file'];
		$userAgent = $_POST['userAgent'];
		$navigatorLanguage = $_POST['navigatorLanguage'];

		$stmt->execute();
		$stmt->close();

		echo "New record created successfully";
	} else{ echo "Something is wrong with you!"; }
?>