
function LeafletOutdoorMapAugmenter(){
	SingleContainerBasedAugmenter.call(this);

	this.buildExpectedView = function(params){ 
		
		var container = this.buildMainContainer(params.id);
			container.appendChild(this.buildMap(params));
		return container;
	}
	this.buildMainContainer = function(elemId){ //I've chosen not to override buildContainer so it can be used at base messages without changing the style

		var container = document.createElement("div");
			container.id = elemId;
			container.style.height = 'auto';
			container.style.width = 'auto';
		return container;
	};
	this.buildMap = function(parameters){

		var container = document.createElement('div');
			container.id = parameters.divId;
			container.style['width'] = '100%';
			container.style['height'] = '230px';

        this.callback = function(params, view){

        	var spaceRep = new LeafletOutdoorMap();
        		params['zoomControl'] = false;
        		params['locateControl'] = false;
        	spaceRep.execute(params);

			var currentMarker = spaceRep.createFixedMarker(
				'current-mowa-marker-' + Date.now(),
				params.currentMarker.lat, params.currentMarker.lon
			);

			for (var i = 0; i < params.targetMarkers.length; i++) {
				var targetMarker = spaceRep.createFixedMarker(
					"target-mowa-marker-" + Date.now(), 
					params.targetMarkers[i].lat, params.targetMarkers[i].lon
				);
	            spaceRep.connectMarkers(currentMarker, targetMarker);
			};
        }
		return container;
	};
	this.checkUserDefinedParameters = function(params){ //CONCRETE

		return true; 
	};
	this.createConfigurationInstance = function(){ //CONCRETE 
	    //TODO: use validationMethods for data validation in the app's view 
	    //If you want to prevent 'undefined', just add a value property with a blank string
	   
	    var cfg = this.createBaseConfiguration(); 
	    	cfg.addParam({ "id": "divId", "value": Date.now()});
		    cfg.addParam({ "id": "zoom", "value": "mowa.mhapp.map.zoom"});
		    cfg.addParam({ "id": "defaultLat", "value": "mowa.mhapp.map.defaultLat"});
		    cfg.addParam({ "id": "defaultLon", "value": "mowa.mhapp.map.defaultLon"});
		    cfg.addParam({ "id": "currentMarker", "value": "mowa.mhapp.map.currentMarker"});
		    cfg.addParam({ "id": "targetMarkers", "value": "mowa.mhapp.map.targetMarkers"});
	    return cfg;
	};
	///////////////////// HARCODING
	this.appendToLocale(this.getProperBundle({ //This should be replicated from augmenters that want's to use localization
		defLanguage: "es",
		bundles: {
			'en':{
				"augmenter.name":"Leaflet Outdoor Map"
			},
			'fr':{
				"augmenter.name":"Leaflet Outdoor Map"
			},
			'es': {
				"augmenter.name":"Leaflet Outdoor Map"
			}
		}
	}));
    this.setClassType("concrete");
    this.setName(this.getLocalized("augmenter.name"));
    this.setClassName("LeafletOutdoorMapAugmenter");
	///////////////////// END OF HARCODING
}