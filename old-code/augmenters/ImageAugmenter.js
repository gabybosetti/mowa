//::MoWA::
	//@name: Image //this should be avoided 
	//@scriptType: MobileWebAugmenter
	//@className: ImageAugmenter
	//@classType: concrete
	//@requiredAugmenters: SingleContainerBasedAugmenter
	//@namespace: mowa
	//@author: lifia
//::MoWA::

function ImageAugmenter(){
	SingleContainerBasedAugmenter.call(this);
	// Concrete class. This class is responsible for adding a formatted div to 
	// the document, within the paragraph received as parameter will be shown.
	this.buildExpectedView = function(params){

		var container = this.buildContainer(params.id);
			container.appendChild(
				this.buildImage(params.imageSrc)
			);
		return container;
	};
	this.buildImage = function(src){
		var img = document.createElement("img");
			img.src = src;
			img.style.width = '90%';
			img.style.maxWidth = '250px';
			img.style.height = 'auto';
			img.style.display = 'block';
			img.style.margin = '10px auto';
			img.style.boxShadow = '0px 0px 7px #888888';
		return img;
	};
	this.checkUserDefinedParameters = function(params){ //Mandatory method, just checks user defined values

	    return (params.imageSrc && params.imageSrc.length>0)? true: false;
	};
	this.createConfigurationInstance = function(){ //Mandatory method
	    //TODO: use validationMethod for data validation in the app's view 
	    var cfg = this.createBaseConfiguration();
		    cfg.addParam({"id": "imageSrc", "userDefined": true, "value": "",
		    	"displayName": this.getLocalized("param.imageSrc.display")});
	    return cfg;
	};
	///////////////////// HARCODING
	this.appendToLocale(this.getProperBundle({ //This should be replicated from augmenters that want's to use localization
		defLanguage: "es",
		bundles: {
			"en":{
				"augmenter.name":"Image",
				"param.imageSrc.display": "Image source"
			},
			"fr":{
				"augmenter.name":"Image",
				"param.imageSrc.display": "Source de l'image"
			},
			"es": {
				"augmenter.name":"Imagen",
				"param.imageSrc.display": "Fuente de la imagen"
			}
		}
	}));
    this.setClassType("concrete");
    this.setName(this.getLocalized("augmenter.name"));
    this.setClassName("ImageAugmenter");
    ///////////////////// END OF HARCODING
}