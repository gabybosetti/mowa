//::MoWA::
	//@scriptType: MobileWebAugmenter
	//@className: PathTracerAugmenter
	//@classType: concrete
	//@requiredAugmenters: SingleContainerBasedAugmenter
	//@namespace: mowa
	//@author: lifia
//::MoWA::

function PathTracerAugmenter(){

	SingleContainerBasedAugmenter.call(this);
	this.buildExpectedView = function(params){ 

		var properSpaceAugmenter;
		eval('properSpaceAugmenter = new ' + params.spaceClass + 'Augmenter();');
		
		var container = properSpaceAugmenter.buildExpectedView(params);
		this.callback = properSpaceAugmenter.callback;

		return container;
	}
	this.checkUserDefinedParameters = function(params){ //CONCRETE

		return true; 
	};
	this.createConfigurationInstance = function(){ //CONCRETE 
	    //TODO: use validationMethods for data validation in the app's view 
	    //If you want to prevent 'undefined', just add a value property with a blank string
	   
	    var cfg = this.createBaseConfiguration(); 
	    	cfg.addParam({ "id": "id", "value":""});
	    	cfg.addParam({ "id": "spaceClass", "value":"mowa.mhapp.map.spaceClass"});

	    	//For maps (Custom and Outdoor shared section)
	    	cfg.addParam({ "id": "divId", "value": Date.now()});
		    cfg.addParam({ "id": "zoom", "value": "mowa.mhapp.map.zoom"});
		    cfg.addParam({ "id": "mapUri", "value": "mowa.mhapp.map.mapUri"});
		    cfg.addParam({ "id": "mapSWLat", "value": "mowa.mhapp.map.mapSWLat"});
		    cfg.addParam({ "id": "mapSWLon", "value": "mowa.mhapp.map.mapSWLon"});
		    cfg.addParam({ "id": "mapNELat", "value": "mowa.mhapp.map.mapNELat"});
		    cfg.addParam({ "id": "mapNELon", "value": "mowa.mhapp.map.mapNELon"});
		    cfg.addParam({ "id": "currentMarker", "value": "mowa.mhapp.map.currentMarker"});
		    cfg.addParam({ "id": "targetMarkers", "value": "mowa.mhapp.map.targetMarkers"});
		    //For maps (Outdoor section)
		    cfg.addParam({ "id": "defaultLat", "value": "mowa.mhapp.map.defaultLat"});
		    cfg.addParam({ "id": "defaultLon", "value": "mowa.mhapp.map.defaultLon"});
		    
	    return cfg;
	};
	///////////////////// HARCODING
	this.appendToLocale(this.getProperBundle({ //This should be replicated from augmenters that want's to use localization
		defLanguage: "es",
		bundles: {
			'en':{
				"augmenter.name":"Path tracer"
			},
			'fr':{
				"augmenter.name":"Chemin traceur"
			},
			'es': {
				"augmenter.name":"Trazador de camino"
			}
		}
	}));
    this.setClassType("concrete");
    this.setName(this.getLocalized("augmenter.name"));
    this.setClassName("PathTracerAugmenter");
	///////////////////// END OF HARCODING
}