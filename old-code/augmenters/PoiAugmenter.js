//::MoWA::
	//@scriptType: MobileWebAugmenter
	//@className: PoiAugmenter
	//@classType: concrete
	//@requiredAugmenters: SingleContainerBasedAugmenter
	//@namespace: mowa
	//@author: lifia
//::MoWA::

function PoiAugmenter(){
	SingleContainerBasedAugmenter.call(this);
	// Concrete class. This class is responsible for adding to document a div 
	// containing a PoI description.
	this.buildExpectedView = function(params){ //SUGGESTED 

		var container = this.buildContainer(params.id);
			container.appendChild(this.buildImage(params.imageSrc));
		    container.appendChild(this.buildDescription(params.description));
		return container;
	};
	this.buildImage = function(imageSrc){

		var contentImg = document.createElement("img");
			contentImg.src = imageSrc;
			contentImg.style.width = '90%';
			contentImg.style.maxWidth = '250px';
			contentImg.style.height = 'auto';
			contentImg.style.display = 'block';
			contentImg.style.margin = '10px auto';
			contentImg.style.boxShadow = '0px 0px 7px #888888';
		return contentImg;
	};
	this.buildDescription = function(description){

		return document.createTextNode(description);
	};
	this.checkUserDefinedParameters = function(params){ //CONCRETE

	    var description = (params.description && 
	    	params.description.length &&
	    	params.description.length > 0)? true: false;
	    var imageSrc = (params.imageSrc && 
	    	params.imageSrc.length &&
	    	params.imageSrc.length > 0)? true: false;

	    return (description && imageSrc)? true: false;
	};
	this.createConfigurationInstance = function(){ //CONCRETE 
	    //TODO: use validationMethods for data validation in the app's view 
	    //If you want to prevent 'undefined', just add a value property with a blank string
	    var cfg = this.createBaseConfiguration();
		    cfg.addParam({"id": "description", "userDefined": true, "value": "",
				"displayName": this.getLocalized("param.description.display")});
		    cfg.addParam({"id": "imageSrc", "userDefined": true, "value": "",
				"displayName": this.getLocalized("param.imageSrc.display")});
	    return cfg;
	};
	///////////////////// HARCODING
	this.appendToLocale(this.getProperBundle({ //This should be replicated from augmenters that want's to use localization
		defLanguage: "es",
		bundles: {
			'en':{
				"augmenter.name":"PoI description",
				"param.description.display": "Description",
				"param.imageSrc.display": "Image source"
			},
			'fr':{
				"augmenter.name":"Description du POI",
				"param.description.display": "Description",
				"param.imageSrc.display": "Source de l'image"
			},
			'es': {
				"augmenter.name":"Descripción del PDI",
				"param.description.display": "Descripción",
				"param.imageSrc.display": "Origen de la imagen"
			}
		}
	}));
    this.setClassType("concrete");
    this.setName(this.getLocalized("augmenter.name"));
    this.setClassName("PoiAugmenter");
	///////////////////// END OF HARCODING
};