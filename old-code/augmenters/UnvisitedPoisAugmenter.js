//::MoWA::
	//@scriptType: MobileWebAugmenter
	//@className: UnvisitedPoisAugmenter
	//@classType: concrete
	//@requiredAugmenters: ListAugmenter
	//@namespace: mowa
	//@author: lifia
//::MoWA::

function UnvisitedPoisAugmenter(){ 
	ListAugmenter.call(this);
	this.buildExpectedView = function(params){

		var container = this.buildContainer(params.id);

		var titleSection = this.buildHeader(this.getLocalized("augmenter.name"));
		if(titleSection) { container.appendChild(titleSection); }
		container.appendChild(this.buildList(params.items));

		return container;
	};
	this.checkUserDefinedParameters = function(params){ //Mandatory method
	    return true; //empty lists are allowed, so...
	};
	this.createConfigurationInstance = function(){ //Mandatory method
	    //TODO: use validationMethod for data validation in the app's view 
	    var cfg = this.createBaseConfiguration();
		    cfg.addParam({"id": "items", "value": "mowa.mhapp.unvisitedPois", 
		    	"displayName": "Items list"});
	    return cfg;
	};
	///////////////////// HARCODING
	this.appendToLocale(this.getProperBundle({ //This should be replicated from augmenters that want's to use localization
		defLanguage: "es",
		bundles: {
			"en":{
				"augmenter.name":"Unvisited POIs",
				"empty.items.collection":"No objects in this collection (you have visited each one in the moment you created it)"
			},
			"fr":{
				"augmenter.name":"Des POIs non visités",
				"empty.items.collection":"Aucun objet dans cette collection (vous avez visité chacun au moment de sa création)"
			},
			"es": {
				"augmenter.name":"PDIs no visitados",
				"empty.items.collection":"No hay objetos en esta colección (has visitado cada uno en el momento en que los creaste)"
			}
		}
	}));
    this.setClassType("concrete");
    this.setName(this.getLocalized("augmenter.name"));
    this.setClassName("UnvisitedPoisAugmenter");
    ///////////////////// END OF HARCODING
}