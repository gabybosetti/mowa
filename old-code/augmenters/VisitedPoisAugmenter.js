//::MoWA::
	//@scriptType: MobileWebAugmenter
	//@className: VisitedPoisAugmenter
	//@classType: concrete
	//@requiredAugmenters: ListAugmenter
	//@namespace: mowa
	//@author: lifia
//::MoWA::

function VisitedPoisAugmenter(){ 
	ListAugmenter.call(this);
	this.buildExpectedView = function(params){

		var container = this.buildContainer(params.id);

		var titleSection = this.buildHeader(this.getLocalized("augmenter.name"));
		if(titleSection) { container.appendChild(titleSection); }
		container.appendChild(this.buildList(params.items));

		return container;
	};
	this.checkUserDefinedParameters = function(params){ //Mandatory method
	    return true; //empty lists are allowed, so...
	};
	this.createConfigurationInstance = function(){ //Mandatory method
	    //TODO: use validationMethod for data validation in the app's view 
	    var cfg = this.createBaseConfiguration();
		    cfg.addParam({"id": "items", "value": "mowa.mhapp.visitedPois", /*visitedPois*/
		    	"displayName": "Items list"});
	    return cfg;
	};
	///////////////////// HARCODING
	this.appendToLocale(this.getProperBundle({ //This should be replicated from augmenters that want's to use localization
		defLanguage: "es",
		bundles: {
			"en":{
				"augmenter.name":"Visited POIs",
				"empty.items.collection":"No objects in this collection"
			},
			"fr":{
				"augmenter.name":"Des POIs visités",
				"empty.items.collection":"Aucun objet dans cette collection"
			},
			"es": {
				"augmenter.name":"PDIs visitados",
				"empty.items.collection":"No hay objetos en esta colección"
			}
		}
	}));
    this.setClassType("concrete");
    this.setName(this.getLocalized("augmenter.name"));
    this.setClassName("VisitedPoisAugmenter");
    ///////////////////// END OF HARCODING
}