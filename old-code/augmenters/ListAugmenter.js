//::MoWA::
	//@scriptType: MobileWebAugmenter
	//@className: ListAugmenter
	//@classType: concrete
	//@requiredAugmenters: SingleContainerBasedAugmenter
	//@namespace: mowa
	//@author: lifia
//::MoWA::

function ListAugmenter(){ //"Enlistador"
	SingleContainerBasedAugmenter.call(this);
	this.buildExpectedView = function(params){

		var container = this.buildContainer(params.id);

		var titleSection = this.buildHeader(params.title);
		if(titleSection) { container.appendChild(titleSection); }
		container.appendChild(this.buildList(params.items));
		
		return container;
	};
	this.buildHeader = function(text){
		if(text && text.length && text.length>0) {
			var spanTag = document.createElement('span');
				spanTag.innerHTML = text;
				spanTag.style['font-weight'] = 'bold';
			return spanTag;
		}
		return;
	};
	this.buildList = function(items){
		// This method is responsible for creating a div containing the description and the item names received as a parameter.

		if(items == null || items.length == 0) {
			return document.createElement('div').appendChild(this.buildEmptyListMessage());
		}
		else return this.buildFullList(items);
	};
	this.buildEmptyListMessage = function(){
		// This method is responsible for creating an HTML span element with a message indicating that 
		// there are no objects in the collection of received items names

		var container = document.createElement('div'),
			spanTag = container.appendChild(document.createElement("span")); 
			spanTag.innerHTML = this.getLocalized("empty.items.collection"); 
			spanTag.style['color'] = '#C23B19';
		return container;
	};
	this.buildFullList = function(items){
		// This method is responsible for creating and add a set of HTML span elements containing 
		// the item name of every received item, in the div received as parameter.
		var container = document.createElement('div');
		for (var i = 0; i < items.length; i++) {
			var spanTag = document.createElement("span"); 
				spanTag.innerHTML = items[i]; 
				spanTag.style.color = '#C23B19';
			container.appendChild(spanTag);
			
			if(i != items.length-1) 
				container.appendChild(document.createTextNode(' - '));
		};
		
		return container;
	};
	this.checkUserDefinedParameters = function(params){ //Mandatory method
	    return true; //empty lists are allowed, so...
	};
	this.createConfigurationInstance = function(){ //Mandatory method
	    //TODO: use validationMethod for data validation in the app's view 
	    var cfg = this.createBaseConfiguration();
		    cfg.addParam({"id": "title", "userDefined": true, "value": "",
		    	"displayName": this.getLocalized("param.title.display")});
		    cfg.addParam({"id": "items", "userDefined": true, "value": "",
		    	"displayName": this.getLocalized("param.items.display")});
	    return cfg;
	};
	///////////////////// HARCODING
	this.appendToLocale(this.getProperBundle({ //This should be replicated from augmenters that want's to use localization
		defLanguage: "es",
		bundles: {
			"en":{
				"augmenter.name":"PoIs List",
				"empty.items.collection":"No objects in this collection",
				"param.title.display": "Title",
				"param.items.display": "Items list"
			},
			"fr":{
				"augmenter.name":"Liste des POIs",
				"empty.items.collection":"Aucun objet dans cette collection",
				"param.title.display": "Titre",
				"param.items.display": "Liste des éléments"
			},
			"es": {
				"augmenter.name":"Lista de PDIs",
				"empty.items.collection":"No hay objetos en esta colección",
				"param.title.display": "Título",
				"param.items.display": "Lista de items"
			}
		}
	}));
    this.setClassType("concrete");
    this.setName(this.getLocalized("augmenter.name"));
    this.setClassName("ListAugmenter");
    ///////////////////// END OF HARCODING
}