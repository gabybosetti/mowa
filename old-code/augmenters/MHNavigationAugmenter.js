//::MoWA::
	//@scriptType: MobileWebAugmenter
	//@className: MHNavigationAugmenter
	//@classType: concrete
	//@requiredAugmenters: SingleContainerBasedAugmenter
	//@namespace: mowa
	//@author: lifia
//::MoWA::

function MHNavigationAugmenter(){
	SingleContainerBasedAugmenter.call(this);

	// Concrete class. This class is responsible for adding to document a div 
	// containing a PoI description.
	this.buildExpectedView = function(params){ 

		this.instructionsSectionId = params.id + '-path-instructions';
		this.pathSectionId = params.id + '-path-tracer';

		var mainContainer = document.createElement("div"),
			container = mainContainer.appendChild(this.buildMainContainer(params.id)),
			poiAugmenterId = params.id + '-PoiAugmenter';

		var self = this,
			wlAugmenter = new LinksAugmenter().buildExpectedView({
				id: poiAugmenterId,
				callback: function(){ 

					//Remove first view elements
					wlAugmenter.remove();

					//Adding the path section
					var cheminTraceur = new PathTracerAugmenter();
					var cheminParametres = JSON.parse(JSON.stringify(params)); //Faster clonning method!
						cheminParametres.id = self.pathSectionId;
						cheminParametres.targetMarkers = [this.relatedItem];
					var cheminVue = cheminTraceur.buildExpectedView(cheminParametres);
					container.appendChild(cheminVue);
					
					//Add second view elements
					var instructions = new TextAugmenter().buildExpectedView({
						id: self.instructionsSectionId,
						description: self.getLocalized("go.to.next.object") + " " + this.relatedItem.name /*this.text*/
					});  
					container.appendChild(instructions);

					//Callback to the Path augmenter for Leaflet components
					cheminTraceur.callback(params, cheminVue);
				},
				items: params.walkingLinks
			});
		container.appendChild(wlAugmenter);

		//container.appendChild(this.buildImage(params.imageSrc));
	    //container.appendChild(this.buildDescription(params.description));
		return mainContainer;
	};
	this.buildMainContainer = function(elemId){ //I've chosen not to override buildContainer so it can be used at base messages without changing the style

		var container = document.createElement("div");
			container.id = elemId;
			container.style.height = 'auto';
			container.style.width = 'auto';
		return container;
	};
	this.checkUserDefinedParameters = function(params){ //CONCRETE

	    return true;
	};
	this.createConfigurationInstance = function(){ //CONCRETE 
	    //TODO: use validationMethods for data validation in the app's view 
	    //If you want to prevent 'undefined', just add a value property with a blank string
	    var cfg = this.createBaseConfiguration(); 
	    	cfg.addParam({ "id": "id", "value":""});

    	//For PathTracerAugmenter
    	cfg.addParam({ "id": "spaceClass", "value":"mowa.mhapp.map.spaceClass"});
    	cfg.addParam({ "id": "walkingLinks", "value": "mowa.mhapp.walkingLinks"});

    	//For maps (Custom and Outdoor shared section)
    	cfg.addParam({ "id": "divId", "value": Date.now()});
	    cfg.addParam({ "id": "zoom", "value": "mowa.mhapp.map.zoom"});
	    cfg.addParam({ "id": "mapUri", "value": "mowa.mhapp.map.mapUri"});
	    cfg.addParam({ "id": "mapSWLat", "value": "mowa.mhapp.map.mapSWLat"});
	    cfg.addParam({ "id": "mapSWLon", "value": "mowa.mhapp.map.mapSWLon"});
	    cfg.addParam({ "id": "mapNELat", "value": "mowa.mhapp.map.mapNELat"});
	    cfg.addParam({ "id": "mapNELon", "value": "mowa.mhapp.map.mapNELon"});
	    cfg.addParam({ "id": "currentMarker", "value": "mowa.mhapp.map.currentMarker"});
	    cfg.addParam({ "id": "targetMarkers", "value": "mowa.mhapp.map.targetMarkers"});
	    //For maps (Outdoor section)
	    cfg.addParam({ "id": "defaultLat", "value": "mowa.mhapp.map.defaultLat"});
	    cfg.addParam({ "id": "defaultLon", "value": "mowa.mhapp.map.defaultLon"});

	    return cfg;
	};
	///////////////////// HARCODING
	this.appendToLocale(this.getProperBundle({ //This should be replicated from augmenters that want's to use localization
		defLanguage: "es",
		bundles: {
			'en':{
				"augmenter.name":"Mobile Hypermedia Navigation",
				"param.description.display": "PoI description",
				"param.imageSrc.display": "PoI's image source",
				"go.to.next.object": "Please, walk to the following object:"
			},
			'fr':{
				"augmenter.name":"Navigation Mobile-Hypermédia",
				"param.description.display": "Description du POI",
				"param.imageSrc.display": "Source de l'image du POI",
				"go.to.next.object": "S'il vous plaît, marchez jusqu'à l'objet suivant:"
			},
			'es': {
				"augmenter.name":"Navegación Hipermedia Móvil",
				"param.description.display": "Descripción del PDI",
				"param.imageSrc.display": "Fuente de la imagen del PDI",
				"go.to.next.object": "Por favor, diríjase al siguiente objeto:"
			}
		}
	}));
    this.setClassType("concrete");
    this.setName(this.getLocalized("augmenter.name"));
    this.setClassName("MHNavigationAugmenter");
	///////////////////// END OF HARCODING
};