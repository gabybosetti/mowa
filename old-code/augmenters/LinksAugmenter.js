//::MoWA::
	//@scriptType: MobileWebAugmenter
	//@className: LinksAugmenter
	//@classType: concrete
	//@requiredAugmenters: ListAugmenter
	//@namespace: mowa
	//@author: lifia
//::MoWA::

function LinksAugmenter(){ 
	//This augmenter is not configurable by the user, you can create a subclass of this and make it wokt properly, but this works just for other augmenters
	ListAugmenter.call(this);
	this.buildExpectedView = function(params){

		var container = this.buildContainer(params.id);
		this.instructionsSectionId = this.id + "_Instructions",
		this.callback = params.callback; //So we can take advantage of inherited existing "listing" methods

		var titleSection = this.buildHeader((params.items.lenght>1)? 
			this.getLocalized("following.pois.description"): 
			this.getLocalized("following.poi.description"));
		if(titleSection) { container.appendChild(titleSection); }
		container.appendChild(this.buildList(params.items)); //calls buildFullList

		return container;
	};
	this.buildFullList = function(items){
		// This method is responsible for creating and add a set of HTML span elements containing 
		// the item name of every received item, in the div received as parameter.
		var container = document.createElement('div'),
			self = this;
		for (var i = 0; i < items.length; i++) {

			var wLink = container.appendChild(document.createElement("a")); 
				wLink.innerHTML = items[i].name;
				wLink.relatedItem = items[i];
				wLink.href = "#";
				wLink.onclick = function(){	return false; };
				wLink.onmouseup = self.callback;

			if(i != items.length-1) 
				container.appendChild(document.createTextNode(' - '));
		};
		
		return container;
	};
	this.createConfigurationInstance = function(){ //Mandatory method
	    //TODO: use validationMethod for data validation in the app's view 
	    var cfg = this.createBaseConfiguration();
		    cfg.addParam({"id": "items", "displayName": "Items list"});
	    return cfg;
	};
	///////////////////// HARCODING
	this.appendToLocale(this.getProperBundle({ //This should be replicated from augmenters that want's to use localization
		defLanguage: "es",
		bundles: {
			"en":{
				"augmenter.name":"Visited PoIs",
				"empty.items.collection":"No objects in this collection",
				"following.poi.description": "Folliwing object:",
				"following.pois.description": "Folliwing objects:"
			},
			"fr":{
				"augmenter.name":"Des POIs visités",
				"empty.items.collection":"Aucun objet dans cette collection",
				"following.poi.description": "Objets suivants:",
				"following.pois.description": "Objet suivant:"
			},
			"es": {
				"augmenter.name":"PDIs visitados",
				"empty.items.collection":"No hay objetos en esta colección",
				"following.poi.description": "Siguiente objeto:",
				"following.pois.description": "Siguientes objetos:"
			}
		}
	}));
    this.setClassType("concrete");
    this.setName(this.getLocalized("augmenter.name"));
    this.setClassName("LinksAugmenter");
    ///////////////////// END OF HARCODING
}