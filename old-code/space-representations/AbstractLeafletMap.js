//::MoWA::
	//@name: Abstract LeafletJs Map
	//@scriptType: SpaceRepresentation
	//@className: AbstractLeafletMap
	//@classType: abstract
	//@namespace: mowa
	//@author: lifia
//::MoWA::
//Rappelez: you shouldn't use prototyping here, otherwise you can't share the object with the window 
//TODO: make inherit from SimpleSpaceRepresentation
function AbstractLeafletMap(){
  
    this.map;
    this.locationControl;
    this.markers = new Array();
    this.originMarker;
    this.targetMarker;
    this.layer_markers = null;
    this.loadLocateControl = function(onlocfound){

        this.locationControl = L.control.locate({
            position: 'topleft',  // set the location of the control
            follow: false,
            stopFollowingOnDrag: true,
            drawCircle: false,  // controls whether a circle is drawn that shows the uncertainty about the location
            setView: true, // automatically sets the map view to the user's location, enabled if `follow` is true
            keepCurrentZoomLevel: true, // keep the current map zoom level when displaying the user's location. (if `false`, use maxZoom)
            onLocationError: function(err) {console.log(err.message)},  // define an error callback function
            onLocationOutsideMapBounds:  function(context) { // called when outside map boundaries
                alert(context.options.strings.outsideMapBoundsMsg);
            },
            icon: 'fa fa-location-arrow',  // class for icon, fa-location-arrow
            showPopup: true, // display a popup when the user click on the inner marker
            strings: {
                title: "Show me where I am",  // title of the locate control
                popup: "You are within {distance} {unit} from this point",  // text to appear if user clicks on circle
                outsideMapBoundsMsg: "You seem located outside the boundaries of the map" // default message for onLocationOutsideMapBounds
            } ,
            locateOptions: {enableHighAccuracy:true}
        }).addTo(this.map);
        
        var self = this;
        if(onlocfound) this.map.on('locationfound', function(evt){
            self.stopUserLocation();
            onlocfound(evt);
        });
    };
    this.stopUserLocation = function(){
        this.locationControl.stop();
    };
    this.execute = function(){}; //ABSTRACT
    this.getRepresentation = function(){
        return this.map;
    };
    this.hasUserDefinedBounds = function(){}; //ABSTRACT
    this.createFixedMarker = function(id, lat, lng) {
        return this.createMarker(id, lat, lng, true); //CHEATING! ;)
    };
    this.createMarker = function(id, lat, lng, fixed) { 
       
        try{
            if (this.layer_markers == null) 
                this.layer_markers = new L.LayerGroup();
        
            var spaceRep = this;
            var marker = new L.Marker(
                L.latLng(lat, lng), 
                { 
                    clickable: (fixed)? false: true, 
                    draggable: (fixed)? false: true, 
                    icon: L.icon({
                        iconUrl: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAApCAYAAADAk4LOAAAGmklEQVRYw7VXeUyTZxjvNnfELFuyIzOabermMZEeQC/OclkO49CpOHXOLJl/CAURuYbQi3KLgEhbrhZ1aDwmaoGqKII6odATmH/scDFbdC7LvFqOCc+e95s2VG50X/LLm/f4/Z7neY/ne18aANCmAr5E/xZf1uDOkTcGcWR6hl9247tT5U7Y6SNvWsKT63P58qbfeLJG8M5qcgTknrvvrdDbsT7Ml+tv82X6vVxJE33aRmgSyYtcWVMqX97Yv2JvW39UhRE2HuyBL+t+gK1116ly06EeWFNlAmHxlQE0OMiV6mQCScusKRlhS3QLeVJdl1+23h5dY4FNB3thrbYboqptEFlphTC1hSpJnbRvxP4NWgsE5Jyz86QNNi/5qSUTGuFk1gu54tN9wuK2wc3o+Wc13RCmsoBwEqzGcZsxsvCSy/9wJKf7UWf1mEY8JWfewc67UUoDbDjQC+FqK4QqLVMGGR9d2wurKzqBk3nqIT/9zLxRRjgZ9bqQgub+DdoeCC03Q8j+0QhFhBHR/eP3U/zCln7Uu+hihJ1+bBNffLIvmkyP0gpBZWYXhKussK6mBz5HT6M1Nqpcp+mBCPXosYQfrekGvrjewd59/GvKCE7TbK/04/ZV5QZYVWmDwH1mF3xa2Q3ra3DBC5vBT1oP7PTj4C0+CcL8c7C2CtejqhuCnuIQHaKHzvcRfZpnylFfXsYJx3pNLwhKzRAwAhEqG0SpusBHfAKkxw3w4627MPhoCH798z7s0ZnBJ/MEJbZSbXPhER2ih7p2ok/zSj2cEJDd4CAe+5WYnBCgR2uruyEw6zRoW6/DWJ/OeAP8pd/BGtzOZKpG8oke0SX6GMmRk6GFlyAc59K32OTEinILRJRchah8HQwND8N435Z9Z0FY1EqtxUg+0SO6RJ/mmXz4VuS+DpxXC3gXmZwIL7dBSH4zKE50wESf8qwVgrP1EIlTO5JP9Igu0aexdh28F1lmAEGJGfh7jE6ElyM5Rw/FDcYJjWhbeiBYoYNIpc2FT/SILivp0F1ipDWk4BIEo2VuodEJUifhbiltnNBIXPUFCMpthtAyqws/BPlEF/VbaIxErdxPphsU7rcCp8DohC+GvBIPJS/tW2jtvTmmAeuNO8BNOYQeG8G/2OzCJ3q+soYB5i6NhMaKr17FSal7GIHheuV3uSCY8qYVuEm1cOzqdWr7ku/R0BDoTT+DT+ohCM6/CCvKLKO4RI+dXPeAuaMqksaKrZ7L3FE5FIFbkIceeOZ2OcHO6wIhTkNo0ffgjRGxEqogXHYUPHfWAC/lADpwGcLRY3aeK4/oRGCKYcZXPVoeX/kelVYY8dUGf8V5EBRbgJXT5QIPhP9ePJi428JKOiEYhYXFBqou2Guh+p/mEB1/RfMw6rY7cxcjTrneI1FrDyuzUSRm9miwEJx8E/gUmqlyvHGkneiwErR21F3tNOK5Tf0yXaT+O7DgCvALTUBXdM4YhC/IawPU+2PduqMvuaR6eoxSwUk75ggqsYJ7VicsnwGIkZBSXKOUww73WGXyqP+J2/b9c+gi1YAg/xpwck3gJuucNrh5JvDPvQr0WFXf0piyt8f8/WI0hV4pRxxkQZdJDfDJNOAmM0Ag8jyT6hz0WGXWuP94Yh2jcfjmXAGvHCMslRimDHYuHuDsy2QtHuIavznhbYURq5R57KpzBBRZKPJi8eQg48h4j8SDdowifdIrEVdU+gbO6QNvRRt4ZBthUaZhUnjlYObNagV3keoeru3rU7rcuceqU1mJBxy+BWZYlNEBH+0eH4vRiB+OYybU2hnblYlTvkHinM4m54YnxSyaZYSF6R3jwgP7udKLGIX6r/lbNa9N6y5MFynjWDtrHd75ZvTYAPO/6RgF0k76mQla3FGq7dO+cH8sKn0Vo7nDllwAhqwLPkxrHwWmHJOo+AKJ4rab5OgrM7rVu8eWb2Pu0Dh4eDgXoOfvp7Y7QeqknRmvcTBEyq9m/HQQSCSz6LHq3z0yzsNySRfMS253wl2KyRDbcZPcfJKjZmSEOjcxyi+Y8dUOtsIEH6R2wNykdqrkYJ0RV92H0W58pkfQk7cKevsLK10Py8SdMGfXNXATY+pPbyJR/ET6n9nIfztNtZYRV9XniQu9IA2vOVgy4ir7GCLVmmd+zjkH0eAF9Po6K61pmCXHxU5rHMYd1ftc3owjwRSVRzLjKvqZEty6cRUD7jGqiOdu5HG6MdHjNcNYGqfDm5YRzLBBCCDl/2bk8a8gdbqcfwECu62Fg/HrggAAAABJRU5ErkJggg==',
                        iconSize: [33, 50],
                        iconAnchor: [16, 50]
                    })
                }
            );
            marker.markerId = id;
            marker.targets = new Array();
            marker.origins = new Array();
            marker.removeOrigin= function(id){

                for (var i = 0; i < this.origins.length; i++) {
                    if(this.origins[i].markerId == id){

                        this.origins.splice(i, 1);
                        break;
                    }
                };
            }
            marker.removeTarget= function(id){

                for (var i = 0; i < this.targets.length; i++) {
                    if(this.targets[i].markerId == id){

                        this.targets.splice(i, 1);
                        break;
                    }
                };
            }
            marker.connectors = new Array(); //draws
            marker.getConnectorById = function(id){
                for (var i = 0; i < this.connectors.length; i++) {
                    if(this.connectors[i].id == id){
                        return this.connectors[i];
                    }
                };
                return;
            }
            marker.removeConnector = function(connector){

                for (var i = 0; i < this.connectors.length; i++) {
                    if(this.connectors[i] == connector){
                        this.connectors.splice(i, 1);
                        return;
                    }
                };
            }
            marker.removeOriginConnectors = function(){
            
                for (var j = 0; j < this.origins.length; j++) {
                    var conn = this.origins[j].getConnectorById('c' + this.markerId);
                    if(conn) {
                        this.origins[j].removeConnector(conn);
                        this._map.removeLayer(conn);
                        this.origins[j].removeTarget(this.markerId);
                    }
                };
            }
            marker.addConnector = function(connector){
                
                this.connectors.push(connector);
            }
            marker.clearConnectors = function(){

                for (var i = 0; i < this.connectors.length; i++) {

                    this.targets[i].removeOrigin(this.markerId); //remove me as a origin
                    this.connectors[i]._map.removeLayer(this.connectors[i]); //remove layer from map
                    //this.removeConnectorById(this.connectors[i].id);
                };
                this.targets = new Array();
                this.connectors = new Array();
            }
            var self = this;
            marker.on('drag', function(e) {
                for (var i = 0; i < this.targets.length; i++) {
                    self.buildPath(this, this.targets[i]);
                };
                for (var i = 0; i < this.origins.length; i++) {
                    self.buildPath(this.origins[i], this);
                };
            });

            this.map.removeLayer(this.layer_markers);
            marker.addTo(this.layer_markers);
            this.map.addLayer(this.layer_markers);

            this.markers.push(marker);
            return marker;
        }catch(err){alert(err.message)}
    };
    this.getMarkers = function(){
        return this.markers;
    };
    this.removeMarker = function(id){

        try{
            for (var i = 0; i < this.markers.length; i++) {
                if(this.markers[i].markerId == id){

                    this.map.removeLayer(this.markers[i]);
                    this.layer_markers.removeLayer(this.markers[i]);
                    this.markers.splice(i,1); //pos, elem quantity
                    break;
                }
            };
        }catch(err){alert(err.message)}
    };
    this.getMarker = function(id){

        for (var i = 0; i < this.markers.length; i++) {
            if(this.markers[i].markerId == id){
                return this.markers[i];
            }
        };
        return;
    };
    this.highlightMarker = function(marker) {

        //Important: Dont use "setIcon", it breaks "mouseout" event
        //Replace by: marker._icon.src = './image.png';
        if(marker) marker._icon.src = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAApCAYAAADAk4LOAAAAAXNSR0IArs4c6QAAAAZiS0dEAP8A/wD/oL2nkwAAAAlwSFlzAAALEwAACxMBAJqcGAAAAAd0SU1FB9wKCg8GGixBl/AAAAZoSURBVFjDnZdtiFxnFcd/9965d173zszd7KaBWKE2qIWC4mr0i2wiq80HFVGpaG2iUMEPYipuK1Q0u5SyIWTp0hSlUAgIJaWhQt4sRgqBaCKrqekmK5HQxdBkZ7M7uzuZl/v+HD/Mvszu7MvUOxyYufc587vnf85zzr1a39Cf6PR48eyRR4H80s9bv/76kdr4b57Y1k/rG7qw6cXhs0MOcAh4AhiwLAt0E4AocFFKlYAzwBsHxq9e2hxypB0yfG7IAAaBF2zHyaWzNqlcrm1dHIa49QfUKxW8RuMd4LkD41cnNoCcXwcYdoBTXcXigO30kEhoqCAmrtdByZq1umVidGUAqFYqzJdKLvD0gfGrp9esExGWbejskCMif7MdZ8Dp7UWPfMJymbi6CCoEojWmApewXCYsV+jK5+ndvTud7up660Lf3p+ugbREYABv5bu7P1ns7iaaXyCu1pp3v62FhLNzpCyLQqEb4JULfXv3r0JEQAQFg7lCYX+xWCSaX0DCEJRqM80wNjyPUkTzC1iWQc/OnYaCU+f69jpNuRB+e27YEZEXCraNajTaAHoqiVksYBYLSNJa+Z6wu9pA4ewc2WyWQqHQIyKDS5EA8P18Pp9L6PqSRKtOZrGAZpqUy2WmpqaY1zWmpqa4XyrhBQFmsdDUpMUnrtcp2DbAT85+7gtGAhGAJ3OZDCoIEFErCTPzeXzf58NSiT0/epovHjyImc0CcPfiX5g4egzb9ykUCwRzc6ulXa9jpdOkUinH87wvGb+v3s4Br/Z2F/RoYR5NRWgSo0mMkcsyv7DIp371HI/+8CkMy1r5I/sTj/BQfz+3Tr2JnTLRREHgrfpmUkRRjOt507qCh03TNFAKomg1D5ZF4HnEO3v5+De/0bYRRYTsx3bz8He+TaVaxUgm10imGg2shIaCx3QRcTRNa0ugblm4fkTx8cc3BDTToCh+9jM0GkFbXiQM0Ztr8zoQiEjzYuuhFBCjgmBTgIgQuy6aRpu/pmmo5jlXV3A3jOO2O4kaDbKmyez4P1BR1AZahty7/FeyyQQSx237KVIKBXd1ESkFUVRTUcQa2TyPhK6TqlW4+cqrbVEATF+5yuKfL5JPp4krlTZIEASIyB3jP4fG5Mu3L38+lTA+nc7nUNU6y10AEbKZDPffu8a992+Q2bWLZHc3lQ+m+OD020wcPUZvNkXSMogrD1b9NA0jl2a6UidW6meJpeZ4olz3vmWnLdBXtVVeDV0pdhW6WPzXNf7+zBW8KMI0DPLJBHucHAldJ7w/09IFwdzRzWK1gR+GF797/dqkjggvfu35d2tBcKVW90g4ztpS9GpE5TkKGYtHdnTx2ENF9vTY9NoZdLfRBLRWJoAo5hsNFAwDJGBlRhwru+7bubTVVimiFOHMTEcj2tzRQ8X3qYfq3SevX7u8fp6cqfjRbS8MSNj2pp12WxPF/IMGIjLS1upf+upgrGC0XHXRTPP/AiQch5ofUAnVP7/3/nsXWyZjS1GInJzzwtkwitEzmY8M0TSNuWoDrSWKNZEgwksDg64ifq1cczHSaTSRji3R69AIIxbC8JYi/uOmM37Jxu55nquW+lfHUcQaszUXERn5wcREvG7GC602MvD8LBh/KPs+utNZAei2jacU5VDdAeON9RW3Rq7VeR+P/rfuxyIClo4YsqXpGYtZz0cRjz5143rQBtlALo5+ZfCWiJxfqPsYTjeI2tT0TIYgVpTcYFZEXt9o72wYydJIHp1xw+2lSmeYqfsAJw7evFnbELJRJCLC0f2/vFSN4ys1P8LY0bPhGj2dIUaY9sKaiIxt1gXaEt9qCmNsxg3Rlofa+igyGUr1AIXxu0OTk5XNIZvJJQLEp+eUuu0qwbDza/aFkUwimsaHfhRAPLpVP9tULhHh2L5fxCIyVvKitj2jd9mUGgEicvLHk5OlLSFbybUk2cnpgNlQBC2TRUTQTBMRYconVhgj23VmfSu1ROD4vp/XNIleK7kRRjYLSmEUitz3IjSJ3nzm3zemtoWwHaX5MH7iTqgClFp5vppWgoKRTmbMtnKBMLrv2RJwshQoDDvPXKCoR3J+dN+zE51BtmcsD8+xadVsI9PNN64RpLMXWl3o7HO8//BkPZIzs4FQieTS8f7Dl6VDSkc5acnN6HS4lIvV9tNJTjo/Xu4/fGkx5vWX+w+/81H8/gdEVI2d1CUrYwAAAABJRU5ErkJggg==';
    };
    this.unhighlightMarker = function(marker) {

        //Important: Dont use "setIcon", it breaks "mouseout" event
        //Replace by: marker._icon.src = './image.png';

        if(marker){
            marker._icon.src = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAApCAYAAADAk4LOAAAGmklEQVRYw7VXeUyTZxjvNnfELFuyIzOabermMZEeQC/OclkO49CpOHXOLJl/CAURuYbQi3KLgEhbrhZ1aDwmaoGqKII6odATmH/scDFbdC7LvFqOCc+e95s2VG50X/LLm/f4/Z7neY/ne18aANCmAr5E/xZf1uDOkTcGcWR6hl9247tT5U7Y6SNvWsKT63P58qbfeLJG8M5qcgTknrvvrdDbsT7Ml+tv82X6vVxJE33aRmgSyYtcWVMqX97Yv2JvW39UhRE2HuyBL+t+gK1116ly06EeWFNlAmHxlQE0OMiV6mQCScusKRlhS3QLeVJdl1+23h5dY4FNB3thrbYboqptEFlphTC1hSpJnbRvxP4NWgsE5Jyz86QNNi/5qSUTGuFk1gu54tN9wuK2wc3o+Wc13RCmsoBwEqzGcZsxsvCSy/9wJKf7UWf1mEY8JWfewc67UUoDbDjQC+FqK4QqLVMGGR9d2wurKzqBk3nqIT/9zLxRRjgZ9bqQgub+DdoeCC03Q8j+0QhFhBHR/eP3U/zCln7Uu+hihJ1+bBNffLIvmkyP0gpBZWYXhKussK6mBz5HT6M1Nqpcp+mBCPXosYQfrekGvrjewd59/GvKCE7TbK/04/ZV5QZYVWmDwH1mF3xa2Q3ra3DBC5vBT1oP7PTj4C0+CcL8c7C2CtejqhuCnuIQHaKHzvcRfZpnylFfXsYJx3pNLwhKzRAwAhEqG0SpusBHfAKkxw3w4627MPhoCH798z7s0ZnBJ/MEJbZSbXPhER2ih7p2ok/zSj2cEJDd4CAe+5WYnBCgR2uruyEw6zRoW6/DWJ/OeAP8pd/BGtzOZKpG8oke0SX6GMmRk6GFlyAc59K32OTEinILRJRchah8HQwND8N435Z9Z0FY1EqtxUg+0SO6RJ/mmXz4VuS+DpxXC3gXmZwIL7dBSH4zKE50wESf8qwVgrP1EIlTO5JP9Igu0aexdh28F1lmAEGJGfh7jE6ElyM5Rw/FDcYJjWhbeiBYoYNIpc2FT/SILivp0F1ipDWk4BIEo2VuodEJUifhbiltnNBIXPUFCMpthtAyqws/BPlEF/VbaIxErdxPphsU7rcCp8DohC+GvBIPJS/tW2jtvTmmAeuNO8BNOYQeG8G/2OzCJ3q+soYB5i6NhMaKr17FSal7GIHheuV3uSCY8qYVuEm1cOzqdWr7ku/R0BDoTT+DT+ohCM6/CCvKLKO4RI+dXPeAuaMqksaKrZ7L3FE5FIFbkIceeOZ2OcHO6wIhTkNo0ffgjRGxEqogXHYUPHfWAC/lADpwGcLRY3aeK4/oRGCKYcZXPVoeX/kelVYY8dUGf8V5EBRbgJXT5QIPhP9ePJi428JKOiEYhYXFBqou2Guh+p/mEB1/RfMw6rY7cxcjTrneI1FrDyuzUSRm9miwEJx8E/gUmqlyvHGkneiwErR21F3tNOK5Tf0yXaT+O7DgCvALTUBXdM4YhC/IawPU+2PduqMvuaR6eoxSwUk75ggqsYJ7VicsnwGIkZBSXKOUww73WGXyqP+J2/b9c+gi1YAg/xpwck3gJuucNrh5JvDPvQr0WFXf0piyt8f8/WI0hV4pRxxkQZdJDfDJNOAmM0Ag8jyT6hz0WGXWuP94Yh2jcfjmXAGvHCMslRimDHYuHuDsy2QtHuIavznhbYURq5R57KpzBBRZKPJi8eQg48h4j8SDdowifdIrEVdU+gbO6QNvRRt4ZBthUaZhUnjlYObNagV3keoeru3rU7rcuceqU1mJBxy+BWZYlNEBH+0eH4vRiB+OYybU2hnblYlTvkHinM4m54YnxSyaZYSF6R3jwgP7udKLGIX6r/lbNa9N6y5MFynjWDtrHd75ZvTYAPO/6RgF0k76mQla3FGq7dO+cH8sKn0Vo7nDllwAhqwLPkxrHwWmHJOo+AKJ4rab5OgrM7rVu8eWb2Pu0Dh4eDgXoOfvp7Y7QeqknRmvcTBEyq9m/HQQSCSz6LHq3z0yzsNySRfMS253wl2KyRDbcZPcfJKjZmSEOjcxyi+Y8dUOtsIEH6R2wNykdqrkYJ0RV92H0W58pkfQk7cKevsLK10Py8SdMGfXNXATY+pPbyJR/ET6n9nIfztNtZYRV9XniQu9IA2vOVgy4ir7GCLVmmd+zjkH0eAF9Po6K61pmCXHxU5rHMYd1ftc3owjwRSVRzLjKvqZEty6cRUD7jGqiOdu5HG6MdHjNcNYGqfDm5YRzLBBCCDl/2bk8a8gdbqcfwECu62Fg/HrggAAAABJRU5ErkJggg==";
            marker._icon.iconSize = [35,35];
        }
    };
    this.connectMarkers = function(originMarker, targetMarker){

        if(!originMarker || !targetMarker) return;

        originMarker.targets.push(targetMarker);
        targetMarker.origins.push(originMarker);

        this.buildPath(originMarker, targetMarker);
    };
    this.buildPath = function(source, target) {

        if(!source || !target) return; //TODO: analize and remove the right eventlistener

        //Remove previous line to redraw
        var connector = source.getConnectorById('c' + target.markerId);
        if (connector){
            source.removeConnector(connector);
            this.map.removeLayer(connector);
        }

        //Draw a new line
        //Draw a new line
        var connector = L.layerGroup();
        var line = L.polyline([source._latlng, target._latlng], {
                    dashArray: '5, 10'
                });
        connector.addLayer(line);
        var arrowHead = L.polylineDecorator(line);
        connector.addLayer(arrowHead);
        arrowHead.setPatterns([
            {   
                offset: '100%', 
                repeat: 0, 
                symbol: L.Symbol.arrowHead({
                    pixelSize: 10, 
                    polygon: false, 
                    pathOptions: {stroke: true}}
                )
            }
        ])

        connector.id = 'c' + target.markerId;
        source.addConnector(connector);
        this.map.addLayer(connector);
    };
    this.setMarkerOnCurrLoc = function(latlng){

        L.marker(latlng).addTo(this.map).bindPopup("You are here").openPopup();
    };
    this.checkRequiredConfiguration = function(params){ //Hook. Implement in subclass
        return false;
    }; 
}