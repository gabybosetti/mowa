//::MoWA::
	//@name: LeafletJs Custom Map
	//@scriptType: SpaceRepresentation
	//@className: LeafletCustomMap
	//@classType: concrete
	//@requiredClasses: AbstractLeafletMap
	//@namespace: mowa
	//@author: lifia
	//@execParam: {"id":"divId", "displayName": "DOM element ID", "validationMethod":"checkDomId"}
	//@cfgParam: {"id":"maxZoom", "displayName": "Max zoom", "validationMethod":undefined}
	//@cfgParam: {"id":"mapUri", "displayName": "Map image URI", "validationMethod":undefined}
	//@cfgParam: {"id":"mapSWLat", "displayName": "South-West bounding latitude", "validationMethod":undefined}
	//@cfgParam: {"id":"mapSWLon", "displayName": "South-West bounding longitude", "validationMethod":undefined}
	//@cfgParam: {"id":"mapNELat", "displayName": "North-East bounding latitude", "validationMethod":undefined}
	//@cfgParam: {"id":"mapNELon", "displayName": "North-East bounding longitude", "validationMethod":undefined}
//::MoWA::
//Warning: don't apply prototyping here, these functions can't be shared in 
//other contexts through cloneInto

function LeafletCustomMap(){

    AbstractLeafletMap.call(this);
    this.map;
    this.markers = new Array();
    this.originMarker;
    this.targetMarker;
    this.layer_markers = null;
    this.execute = function(params){
        try{ 
            if(params.zoomControl == undefined || params.zoomControl == null || params.zoomControl != false) params.zoomControl = true;
            if(params.locateControl == undefined || params.locateControl == null || params.locateControl != false) params.locateControl = false;

            var centerPoint;
            if(params.centerLat && params.centerLon && params.centerLat.length > 1 && params.centerLon.length > 1){
                centerPoint = [params.centerLat, params.centerLon];
            }else{
                centerPoint = [params.mapSWLat, params.mapSWLon];
            }
            this.map = this.createCustomMap(
                params.divId,
                params.mapUri,
                params.zoom, 
                [
                    [params.mapSWLat, params.mapSWLon],
                    [params.mapNELat, params.mapNELon]
                ],
                params.zoomControl,
                centerPoint
            );  
            if(!params.onlocfound) params.onlocfound = undefined; //in case property doesn't exist
            if(params.locateControl) this.loadLocateControl(params.onlocfound);
        }
        catch(err){alert(err.message)}
    };
    this.hasUserDefinedBounds = function(){
        return true;
    };
    this.createCustomMap = function(id, mapUri, zoom, imgBounds, zoomControl, defView){

        try{
            var map = L.map(id, {
                zoomAnimation: false,
                fadeAnimation: false,
                inertia: false,
                touchZoom: false,
                zoomControl: zoomControl
            });
            L.imageOverlay(mapUri, imgBounds).addTo(map);
            map.setMaxBounds(imgBounds);
            map.setView(
                defView,
                zoom
            ); 
            return map;
        }
        catch(err){alert(err.message)}
    };
    this.checkMapImage = function(params){

        var img = document.createElement("img"); 
        img.src = params.url;
    }
    this.checkRequiredConfiguration = function(params){
        return (params.mapUri && params.mapUri !="" &&
            params.mapSWLat && params.mapSWLat !="" &&
            params.mapSWLon && params.mapSWLon !="" &&
            params.mapNELat && params.mapNELat !="" &&
            params.mapNELon && params.mapNELon !=""
            )? true: false;
    };
}