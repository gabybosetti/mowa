//::MoWA::
	//@name: LeafletJs Outdoor Map
	//@scriptType: SpaceRepresentation
	//@className: LeafletOutdoorMap
	//@classType: concrete
	//@requiredClasses: AbstractLeafletMap
	//@namespace: mowa
	//@author: lifia
	//@execParam: {"id":"elemId", "displayName": "DOM element ID", "validationMethod":"checkDomId"}
//::MoWA::

function LeafletOutdoorMap(enableAuthoring){

    AbstractLeafletMap.call(this);

    this.map;
    this.markers = new Array();
    this.execute = function(params){

        try{
            this.lastExecutedParams = params; //TODO: this is just for solving clearConnections problem
            if(params.zoomControl == undefined || params.zoomControl == null || params.zoomControl != false) params.zoomControl = true;
            if(params.locateControl == undefined || params.locateControl == null || params.locateControl != false) params.locateControl = true;
            this.createOutdoorMap(params.divId,  params.zoomControl);
            
            if(params.locateControl) {
                if(!params.onlocfound) params.onlocfound = undefined; //in case property doesn't exist
                this.loadLocateControl(params.onlocfound);
            }

            if(params.defaultLat && params.defaultLon){ /*default from the app PoV, not a starting value*/
                this.map.setView(new L.LatLng(params.defaultLat, params.defaultLon), params.zoom); 
            }
            else {
                this.map.setView(new L.LatLng('-34.90381980927888', '-57.935972213745124'), params.zoom); 
            }
        }
        catch(err){console.log(err.message);}
    };
    this.hasUserDefinedBounds = function(){
        return false;
    };
    this.createOutdoorMap = function(divId,  zoomControl){

        this.map = L.map(divId, {
            zoomAnimation: false,
            fadeAnimation: false,
            inertia: false,
            touchZoom: false,
            zoomControl: zoomControl
        });
        // add an OpenStreetMap tile layer
        L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png').addTo(this.map);
        return this.map;
    }
    this.checkRequiredConfiguration = function(params){

        return (params.defaultLat && params.defaultLon)? true: false;
    };
}