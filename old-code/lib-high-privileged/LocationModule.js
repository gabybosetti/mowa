/**
 * Represents a specific position or point in a space representation.
 * @constructor
 */
function Location(){

	this.equals = function(locationAsVisitor){};
	this.equalsToQrLocation = function(locationAsVisitor){
		return false;
	};
	this.equalsToGpsLocation = function(locationAsVisitor){
		return false;
	}
	this.appliesToSameSpaceRepresentation = function(locationAsVisitor){};
	this.appliesToQrSpaceRepresentation = function(location){
		return false;
	}
	this.appliesToGpsSpaceRepresentation = function(location){
		return false;
	}
	this.accept = function(visitor, arguments){}
}

/**
 * Represents a specific position in a space of representation for QRs codes.
 * @constructor
 * @kind Location
 * @param {String} url - The url associated with the location
 */
function QRLocation(url){
	Location.call(this);
	this.url = url;
	this.getUrl = function(){
		return this.url;
	};
	this.equals = function(locationAsVisitor){
		return locationAsVisitor.equalsToQrLocation(this);
	};
	this.equalsToQrLocation = function(location){

		var urlA = location.getUrl(); // http://es.m.wikipedia.org/wiki/Vaca_%C3%B1ata
		var urlB = this.getUrl(); // http://es.m.qrwp.org/Vaca_%25C3%25B1ata

		if (urlA == urlB)
			return true;

		var urlAVariations = [
			urlA,
			decodeURI(urlA), 
			unescape(decodeURI(urlA)),
			decodeURI(decodeURI(urlA))
		];

		var urlBVariations = [
			urlB,
			decodeURI(urlB), 
			unescape(decodeURI(urlB)),
			decodeURI(decodeURI(urlB))
		];

		for (var i = 0; i < urlAVariations.length; i++) {
			for (var j = 0; j < urlBVariations.length; j++) {
				if(urlAVariations[i] == urlBVariations[j])
					return true;
			};
		};

		return false;
	};
	this.appliesToSameSpaceRepresentation = function(locationAsVisitor){
		return locationAsVisitor.appliesToQrSpaceRepresentation(this);
	};
	this.appliesToQrSpaceRepresentation = function(location){
		return true;
	};
	this.accept = function(visitor, arguments){
		visitor.visitQRLocation(this, arguments);
	};
}

/**
 * Represents a specific position in the geographic coordinate system.
 * @constructor
 * @kind Location
 * @param {Number} latitude - The latitude value expressed in decimal format. E.g. -34.915215
 * @param {Number} longitude - The longitude value expressed in decimal format. E.g. -57.947823
 * @param {Integer} proximityFactor - The number of metres, from the central location, that is considered a valid location to activate the POI
 */
function GpsLocation(latitude, longitude, proximityFactor){
	Location.call(this);

	this.proximityFactor = proximityFactor || 50;
	this.latitude = latitude;
	this.longitude = longitude;
	this.getLatitude = function(){
		return this.latitude;
	};
	this.getLongitude = function(){
		return this.longitude;
	};
	this.getProximityFactor = function(){
		return (this.proximityFactor/100000);
	};
	this.equals = function(locationAsVisitor){
		return locationAsVisitor.equalsToGpsLocation(this);
	};
	this.equalsToGpsLocation = function(locationAsVisitor){
		return this.nearTo(locationAsVisitor);
	};
	this.nearTo = function(locationAsVisitor){

		var lonDifToSqare = window.Math.pow((this.getLongitude() - locationAsVisitor.getLongitude()),2);
		var latDifToSqare = window.Math.pow((this.getLatitude() - locationAsVisitor.getLatitude()),2);

		var distance = window.Math.abs(window.Math.sqrt(lonDifToSqare + latDifToSqare));

		//window.NativeWindow.toast.show("Distancia: " + distance, "short");

		//Si la distancia es inferior o igual a 50 mt entonces si está cerca.
		if(distance <= this.getProximityFactor())
			return true;
		else return false;
	};
	this.appliesToSameSpaceRepresentation = function(locationAsVisitor){
		return locationAsVisitor.appliesToGpsSpaceRepresentation(this);
	};
	this.appliesToGpsSpaceRepresentation = function(location){
		return true;
	};
	this.accept = function(visitor, arguments){
		visitor.visitGpsLocation(this, arguments);
	};
}

/**
 * Represents a specific position in a bidimensional coordinate system.
 * @constructor
 * @kind Location
 * @param {Number} xAxis - The X axis value. 
 * @param {Number} yAxis - The Y axis value.
 */
function CustomMapLocation(xAxis, yAxis){
	Location.call(this);
	this.xAxis = xAxis;
	this.yAxis = yAxis;
	this.getXAxis = function(){
		return this.xAxis;
	};
	this.getYAxis = function(){
		return this.yAxis;
	};
	this.equals = function(locationAsVisitor){
		return locationAsVisitor.equalsToCustomMapLocation(this);
	};
	this.equalsToCustomMapLocation = function(locationAsVisitor){
		return (this.getXAxis == locationAsVisitor.getXAxis && this.getYAxis == locationAsVisitor.getYAxis);
	};
	this.appliesToSameSpaceRepresentation = function(locationAsVisitor){
		return locationAsVisitor.appliesToCustomMapSpaceRepresentation(this);
	};
	this.appliesToCustomMapSpaceRepresentation = function(location){
		return true;
	};
	this.accept = function(visitor, arguments){
		visitor.visitCustomMapLocation(this, arguments);
	};
}

function LocationsHistory(){
	this.locations = new Array();
	this.addLocation = function(aLocation){
		var addLocation = true;
		for (var i = 0; i < this.locations.length; i++) {
			if(this.locations[i].equals(aLocation))
			{
				addLocation = false; 
				break;
			}
		};	
		if(addLocation) this.locations.push(aLocation);
	};
	this.lastLocation = function(){
		return this.locations[this.locations.length-1];
	};
	this.clearHistory = function(){
		//This method clear all objects used by the history 
		this.locations = new Array();
	};
	this.getLocations = function(){
		return this.locations;
	}
}