function MobileApplication(name){
	//Abstract class. This class represents the mobile application.
	this.setName(name);
	this.user = new User();
	this.poiManager = new PoiManager();
	this.mainPoi;
	this.augManager;
};
MobileApplication.prototype = new ScriptInstance();
MobileApplication.prototype.setName = function(name){

	this.name = name;
}
MobileApplication.prototype.setAugmentersManager = function(augManager){
	
	this.augManager = augManager;
};
MobileApplication.prototype.getMainPoi = function(){

	return this.mainPoi;
};
MobileApplication.prototype.load = function(poi, sensedLocation){
	//This method is called when adaptation must be loaded

	this.user.setCurrentLocation(sensedLocation);
	this.setLocationAsVisited(sensedLocation);
};
MobileApplication.prototype.getName = function(){
	//This method is called when adaptation must be loaded

	return this.name;
};
MobileApplication.prototype.getReady = function(callbackOnComplete){

	this.getCurrentPoi().getReady(callbackOnComplete);
};
MobileApplication.prototype.usePoiDescriptor = function(descriptorName){
	// This method allows to load an existing description of poi
	//
};
MobileApplication.prototype.getAugmenterInstance = function(augmenterName){
	// This method allows to get a, augmenter instance. 

	return this.augManager.getAugmenter(augmenterName);
	//return eval("new " + augmenterName + "()"); 
};
MobileApplication.prototype.createLeafPoi = function(name, url, location, spaceRep, extraProps, externalProps){

	var newPoi = new LeafPoi(name, url, location, spaceRep, extraProps, externalProps);
	this.poiManager.addPoiToCollection(newPoi);

	return newPoi;
};
MobileApplication.prototype.createCompositePoi = function(name, url, location, spaceRep, extraProps, externalProps, containedPois){

	var newPoi = new CompositePoi(name, url, location, spaceRep, containedPois, extraProps, externalProps);
	this.poiManager.addPoiToCollection(newPoi);

	return newPoi;
};
MobileApplication.prototype.setLocationAsVisited = function(location){
	// This method allows you to mark a location as visited

	this.user.locationsHistory.addLocation(location);
};
MobileApplication.prototype.getVisitedPois = function(){
	// This method returns a collection of visited locations. If it doesn't get any object, it returns null.

	var registeredLocations = this.user.locationsHistory.getLocations();

	var visitedPois = new Array();

	for (var i = 0; i < registeredLocations.length; i++) {

		var poi = this.poiManager.getPoiFromLocation(registeredLocations[i]);
		if (poi != null && poi != this.getCurrentPoi()) 
			visitedPois.push(poi);	
	};

	return visitedPois;
};
MobileApplication.prototype.getUnvisitedPois = function(){
	// This method returns a collection of unvisited locations. If it doesn't get any object, it returns null.

	var pois = this.poiManager.getPois().slice(0);
	var visitedPois = this.getVisitedPois();
	var unvisitedPois = new Array();

	while(pois.length != 0)
	{
		var addUnvisited = true;
		
		if(pois[0] == this.getCurrentPoi()) 
			addUnvisited = false;
		else
			for (var i = 0; i < visitedPois.length; i++) {
				if(visitedPois[i] == pois[0])
				{
					addUnvisited = false;
					break;
				}
			};

		if(addUnvisited) unvisitedPois.push(pois[0]);

		pois.shift();
	}
	
	return (unvisitedPois.length != 0)? unvisitedPois : null;
};
MobileApplication.prototype.getCurrentPoi = function(){
	// This method returns the current poi.
	
	return this.poiManager.getPoiFromLocation(this.user.getCurrentLocation());
};
MobileApplication.prototype.clearData = function(){
	// This method resets the values stored by previous executions of the application.
	this.clearVisitedPois();
}
MobileApplication.prototype.clearVisitedPois = function(){
	//This method clear the "visitedPois" variable in localstorage

	this.user.locationsHistory.clearHistory();
};
/**
 * This method creates an instance of CompositePoi and assigns it as main poi 
 * in application (it is also added to the application poiManager)
 * @method
 * @public 
 * @memberof MobileApplication
 */
MobileApplication.prototype.createMainPoi = function(name, url, location, spaceRep, extraProps, externalProps, containedPois){

	this.mainPoi = this.createCompositePoi(
		name, 
		url, 
		location, 
		spaceRep, 
		extraProps, 
		containedPois
	); 

	return this.mainPoi;
};

function StorageProperty(key, value){

	this.key = key;
	this.value = value;
}
StorageProperty.prototype.getKey = function(){  

	return this.key;
}
StorageProperty.prototype.setValue = function(value){  

	this.value = value;
}
StorageProperty.prototype.getValue = function(){  

	return this.value; 
}

function StorageManager(){ 	

	if (arguments.callee.instance)
		return arguments.callee.instance;
	arguments.callee.instance = this;

	this.storageProperties = new Array();
};
StorageManager.prototype.propertyExists = function(key){  

	for (var i = 0; i < this.storageProperties.length; i++) {
		console.log(this.storageProperties[i].getKey() + " == " + key);
		if(this.storageProperties[i].getKey() == key)
			return true;
	};

	return false;
};
StorageManager.prototype.getProperty = function(key){  

	for (var i = 0; i < this.storageProperties.length; i++) {
		if(this.storageProperties[i].getKey() == key)
			return this.storageProperties[i];
	};

	return null;
};
StorageManager.prototype.createEmptyProperty = function(key){  

	var property = new StorageProperty(key,null);
	this.storageProperties.push(property);

	return property;
};
StorageManager.prototype.saveValue = function(key, value){  

	var property = (this.propertyExists(key))? this.getProperty(key): this.createEmptyProperty(key);
	property.setValue(value);
	console.log("Valor almacenado: " + property.getValue());
};
StorageManager.prototype.restoreValue = function(key){
	//This method allows to retrieve a value using the specified key

	console.log("Existe?" + this.propertyExists(key));
	var property = (this.propertyExists(key))? this.getProperty(key): this.createEmptyProperty(key);
	console.log("Valor recuperado: " + property.getValue());
	return property.getValue();
};
StorageManager.prototype.clearValue = function(key){

	this.removeProperty(key);
};
StorageManager.prototype.removeProperty = function(key){

	for (var i = 0; i < this.storageProperties.length; i++) {
		if(this.storageProperties[i].getKey() == key)
		{
			this.storageProperties.splice(i,1);
			break;
		}
	};
};

/**
 * Represents a user of a MoWA application
 * @constructor
 */
function User(){
	
	this.locationsHistory = new LocationsHistory();
	this.currentLocation;
};
User.prototype.setCurrentLocation = function(location){

	this.currentLocation = location;
};
User.prototype.getCurrentLocation = function(){

	return this.currentLocation;
};
User.prototype.getLocationsHistory = function(){

	return this.locationsHistory;
};
User.prototype.getLastVisitedLocation = function(){
	
	if(this.getLocationsHistory().getLocations())
	{
		var lastLocationIndex = this.getLocationsHistory().getLocations().length -1;
		return this.getLocationsHistory().getLocations()[lastLocationIndex];
	}
	else return null;
};
User.prototype.getPreviousLocation = function(location){

	var locations = this.getLocationsHistory().getLocations();

	for (var i = locations.length - 1; i > 0; i--) {
		if(locations[i] == location)
			return locations[i-1];
	};

	return null;
};

function PoiManager(){

	this.pois = new Array();
};
PoiManager.prototype.addPoiToCollection = function(poi){

	if(!this.isThisPoiInCollection(poi))
		this.pois.push(poi);
};
PoiManager.prototype.isThisPoiInCollection = function(poi){

    return (this.pois.indexOf(poi) != -1);
};
PoiManager.prototype.getPoiFromLocation = function(location){ //HERE

	for (var i = 0; i < this.pois.length; i++) 
	{
		if(this.pois[i].getPhysicalLocation().equals(location))
			return this.pois[i];
	}
	
	return null;
};
PoiManager.prototype.getPoiFromURL = function(url){
	
	if(typeof(url) == "undefined")
		return null;
	
	for(var i=0; i<this.pois.length; i++)
		if(this.pois[i].getUrl() == decodeURI(url))
			return this.pois[i];
	
	return null;
};
PoiManager.prototype.getPois = function(){
	
	return this.pois;
};


function PointOfInterest(name, url, location){
	
	this.name = name;
	this.url = url;
	this.poiProperties = new Array();
	this.location = location;
	this.propertiesToBeRetrieved = 0;
};
PointOfInterest.prototype.getName = function(){
	return this.name;
};
PointOfInterest.prototype.getUrl = function(){
	return this.url;
};
PointOfInterest.prototype.createProperty = function(key, value){

	if(this.propertyExists(key))
	{
		window.NativeWindow.toast.show(
			MoWA.getLanguageBundle("AppMod").formatStringFromName("propertyAlreadyExists", [key], 1), 
			"short");

		var property = this.getProperty(key);
		property.value = value;
	}
	else this.poiProperties.push(new PoiProperty(key, value));
};
PointOfInterest.prototype.createPropertyFrom = function(key, url, xpathExpression){

	var contentRequester = new ContentRequester();
	contentRequester.getContentFromUrl(key, this, url, xpathExpression);
	this.increasePropertiesToBeRetrieved();
};
PointOfInterest.prototype.getPropertiesToBeRetrieved = function(){

	return this.propertiesToBeRetrieved;
}
PointOfInterest.prototype.increasePropertiesToBeRetrieved = function(){

	this.propertiesToBeRetrieved++;
};
PointOfInterest.prototype.decreasePropertiesToBeRetrieved = function(){
	
	this.propertiesToBeRetrieved--;
};
PointOfInterest.prototype.setProperty = function(key, value){
	
	if(this.propertyExists(key))
	{
		var property = this.getProperty(key);
		property.value = value;
	}
	else window.NativeWindow.toast.show(
		MoWA.getLanguageBundle("AppMod").formatStringFromName("propertyDoesntExist", [key], 1), 
		"short");
};
PointOfInterest.prototype.propertyExists = function(key){
	// This method checks if a property, composed by the key received as parameter, has already been created.

	for(var property in this.poiProperties)
		if(property.key == key)
		{
			console.log("'" + key + "' property doesn't exist.");
			return true;
		}

	return false;
}
PointOfInterest.prototype.getProperty = function(key){

	for (var i = 0; i < this.poiProperties.length; i++) 
		if(this.poiProperties[i].getKey() == key)
			return this.poiProperties[i].getValue();
	
	return null;
};
PointOfInterest.prototype.addContainedPoi = function(poi){}; //Abstract
PointOfInterest.prototype.getContainedPois = function(){}; //Abstract
PointOfInterest.prototype.getPhysicalLocation = function(){
	
	return this.location;
};
PointOfInterest.prototype.getSpaceRepresentation = function(){}; //Abstract
PointOfInterest.prototype.getReady = function(callbackOnComplete){

	if(this.getPropertiesToBeRetrieved() > 0)
	{
		var self = this;
		window.content.setTimeout(
			function() {
				self.getReady(callbackOnComplete);
			}
		,1000);
	}
	else callbackOnComplete();
};
PointOfInterest.prototype.equalsTo = function(poi){

	return (this.getName() == poi.getName());
};
PointOfInterest.prototype.addProperties = function(extraProps, externalProps){

	for (property in extraProps)
	{
		this.createProperty(property, extraProps[property]);
	}

	for (property in externalProps)
	{
		this.createPropertyFrom(
			property, 
			externalProps[property].url, 
			externalProps[property].xpathExpression
		);
	};
}

function LeafPoi(name, url, location, parentPoi, extraProps, externalProps){ 

	PointOfInterest.prototype.constructor.call(this);

	this.name = name;
	this.url = url;
	this.poiProperties = new Array();
	this.location = location;
	this.parentPoi = parentPoi;

	this.addProperties(extraProps, externalProps);
};
LeafPoi.prototype = new PointOfInterest();
LeafPoi.prototype.setParentPoi = function(parentPoi){
	this.parentPoi = parentPoi;
};
LeafPoi.prototype.getSpaceRepresentation = function(){

	return (this.parentPoi)? this.parentPoi.getSpaceRepresentation() : null;
};
LeafPoi.prototype.getMarker = function(){
	
	return this.getSpaceRepresentation().getMarkerFromPoi(this);
};

function CompositePoi(name, url, location, spaceRep, containedPois, extraProps, externalProps){
	
	PointOfInterest.prototype.constructor.call(this);

	this.name = name;
	this.url = url;
	this.poiProperties = new Array();
	this.location = location;
	this.spaceRep = spaceRep;
	this.containedPois = (containedPois == null || containedPois == "undefined")? new Array() : containedPois;
	this.addProperties(extraProps, externalProps);
};
CompositePoi.prototype = new PointOfInterest();
CompositePoi.prototype.addContainedPoi = function(poi){
	
	poi.setParentPoi(this);
	this.containedPois.push(poi);
};
CompositePoi.prototype.removeContainedPoi = function(poi){
	
	//remove
};
CompositePoi.prototype.getContainedPois = function(){
	return this.containedPois;
};
CompositePoi.prototype.getSpaceRepresentation = function(){
	
	return this.spaceRep;
};
CompositePoi.prototype.setSpaceRepresentation = function(spaceRep){
	
	this.spaceRep = spaceRep;
};
CompositePoi.prototype.getMarker = function(){
	
	return this.spaceRep.getMarkerFromPoi(this);
};

function PoiProperty(key, value){
	
	this.key = key;
	this.value = value;
};
PoiProperty.prototype.getKey = function(){
	return this.key;
};
PoiProperty.prototype.getValue = function(){

	return this.value;
};