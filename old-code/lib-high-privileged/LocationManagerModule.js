/**
 * Represents a locations manager which can read and decode different kind of positioning objects.
 * @constructor
 * @abstract
 */

function Sensor(){
	this.listeners = new Array();
}
Sensor.prototype.isSupported = function(listener){}
Sensor.prototype.addListener = function(listener){
	this.listeners.push(listener);
}
Sensor.prototype.startDetection = function(){}
Sensor.prototype.onPositionSensed = function(retrievedData){}
Sensor.prototype.removeListener = function(listener){

	for (var i = 0; i < this.listeners.length; i++) {
		if(this.listeners[i] == listener)
		{
			this.listeners.splice(i,1);
			break;
		}
	};
}
Sensor.prototype.stopDetection = function(){

	return null;
}

/*
function Sensor(){
	this.listeners = new Array();
}
Sensor.prototype.isSupported = function(listener){}
Sensor.prototype.disable = function(listener){}
Sensor.prototype.addListener = function(listener){
	this.listeners.push(listener);
}
Sensor.prototype.startDetection = function(){}
Sensor.prototype.onPositionSensed = function(retrievedData){}
Sensor.prototype.removeListener = function(listener){

	for (var i = 0; i < this.listeners.length; i++) {
		if(this.listeners[i] == listener){
			this.listeners.splice(i,1);
			break;
		}
	};
}
Sensor.prototype.stopDetection = function(){}
*/



function BarcodesManager(){
	this.maxImgSize = 170*170;
}
BarcodesManager.prototype = new Sensor();



function QrCodesManager(){
	this.maxImgSize = 170*170;
}
QrCodesManager.prototype = new BarcodesManager();
QrCodesManager.prototype.startDetection = function(){

	if(this.isStreamSupported())
		this.showScanner();
	else 	
		window.NativeWindow.toast.show(
			MoWA.getLanguageBundle("LocManMod").GetStringFromName("webRtcNotSupported"),
			"long");
}
QrCodesManager.prototype.onPositionSensed = function(retrievedData){

	try{
		window.NativeWindow.toast.show(
			MoWA.getLanguageBundle("LocManMod").GetStringFromName("qrWasDecoded"), 
			"short");

		for (var i = 0; i < this.listeners.length; i++) {

			this.listeners[i].updateQRLocation(retrievedData);
		};
		
	}catch(err){window.NativeWindow.toast.show(err.message, "long");}
}
QrCodesManager.prototype.isStreamSupported = function() {

	window.navigator.getMedia = window.navigator.mozGetUserMedia;
  	
  	if (window.navigator.getMedia)
    	return true;
  	else
    	return false;
}
QrCodesManager.prototype.showScanner = function(){

	try{
		var manager = this;
		var linkGui = function(){

			var btnScreenshot = window.content.document.getElementById("screenshotButton");
			var video = window.content.document.getElementById("liveVideo");
			var canvas = window.content.document.getElementById("qrCanvas");

			//Get the live video
			window.navigator.getMedia( 
				//constraints
				{video: true, audio: false}, 
				//sucessCallback
				function(stream) {
					if (window.navigator.mozGetUserMedia) 
						video.mozSrcObject = stream;
					else 
					{
						var vendorURL = window.URL || window.webkitURL;
						video.src = vendorURL.createObjectURL(stream);
					}
					video.play();
				},
				//errorCallback
				function(err) {
					window.NativeWindow.toast.show(err.message, "short");
				}
			);

			btnScreenshot.onmouseup = function(){
				manager.takeSnapshot(video, canvas);
			}	

			window.removeEventListener("DOMContentLoaded", linkGui, false); //Se quita el listener
		}

		window.addEventListener("DOMContentLoaded", linkGui, false); 

		window.BrowserApp.loadURI("chrome://MoWA/content/data/QRScanner.html");
    }
	catch(err) {window.NativeWindow.toast.show(err.message,"long");}	
}
QrCodesManager.prototype.takeSnapshot = function(video, canvas) {

	try{
		video.pause();
		//window.NativeWindow.toast.show("Starting decode", "short");

		this.drawImage(video, canvas);
		var qrCode = new QrCode(canvas);
		var retrievedData = this.decodeBarcode(qrCode);

		//window.NativeWindow.toast.show("Retrieved", "short");

		if(retrievedData == null)
			video.play();
		else
		{
			video.pause();
			video.mozSrcObject=null; //video.src = null;
			window.navigator.getMedia = null;

			this.onPositionSensed(retrievedData);
		}
		
	}catch(err){window.NativeWindow.toast.show(err.message, "short");}
}
QrCodesManager.prototype.drawImage = function(video, canvas){

	var nheight = video.videoHeight;
    var nwidth = video.videoWidth;
    
    if(nwidth*nheight>this.maxImgSize)
    {
    	var ir = nwidth/nheight;
		nheight = Math.sqrt(this.maxImgSize/ir);
		nwidth = ir*nheight;
	}

	canvas.width = nwidth;
	canvas.height = nheight;

	var context = canvas.getContext('2d');
	context.clearRect(0, 0, nwidth, nheight); //context.clearRect(0, 0, size, size);
	context.drawImage(video, 0, 0, nwidth, nheight); //context.drawImage(video, 0, 0, size, size);
}
QrCodesManager.prototype.decodeBarcode = function(qrCode) {
	
	try{
		var ctx = qrCode.canvas.getContext('2d');
		var image = qrCode.grayScaleToBitmap(qrCode.grayscale());
			
		var detector = new Detector(image);
		var qRCodeMatrix = detector.detect(qrCode);
		
		var reader = new Decoder().decode(qRCodeMatrix.bits);
		var data = reader.DataByte(qrCode);
		var str="";
		for(var i=0;i<data.length;i++)
		{
			for(var j=0;j<data[i].length;j++)
				str+=String.fromCharCode(data[i][j]);
		}
	
		return str; //decodeURIComponent(escape(str));
	}
	catch(err){
		window.NativeWindow.toast.show(
			MoWA.getLanguageBundle("LocManMod").GetStringFromName("qrWasNotDecoded"),
			"short");
	}

	return null;
}









function GpsManager(){
	this.handlerID = null;
}
GpsManager.prototype = new Sensor();
GpsManager.prototype.isGeolocationSupported = function() {

	if(!window.navigator)
		return false;

	if ("geolocation" in window.navigator) 
		return true; 
	else return false; 
}
GpsManager.prototype.onPositionSensed = function(retrievedData){

	window.NativeWindow.toast.show(
		MoWA.getLanguageBundle("LocManMod").GetStringFromName("locationWasDetected"), 
		"short");

	for (var i = 0; i < this.listeners.length; i++) {
		this.listeners[i].updateGpsLocation(retrievedData);
	};
}
GpsManager.prototype.startDetection = function(){	

	if(this.isGeolocationSupported())
		this.watchCurrentPosition();
	else window.NativeWindow.toast.show(
		MoWA.getLanguageBundle("LocManMod").GetStringFromName("geolocationNotSupported"),
		"long");
}
GpsManager.prototype.watchCurrentPosition = function(){

	if(this.handlerID != null){
		window.navigator.geolocation.clearWatch(this.handlerID);
		this.handlerID = null;
	}

	var gpsManager = this;
	function geoSuccess(position) {

		var retrievedLocation = new Array(position.coords.latitude, position.coords.longitude);
		gpsManager.onPositionSensed(retrievedLocation);
	};
	function geoError() {};
	var geoOptions = {
		enableHighAccuracy: true, // If true --> slower response times and increased power consumption
		maximumAge        : 300, 	//30000, //max age in ms of cached position that is acceptable to return
		timeout           : 15000	//27000 //max length of time (ms) the device is allowed to return a position
	};

	this.handlerID = window.navigator.geolocation.watchPosition(geoSuccess, geoError, geoOptions);
}
GpsManager.prototype.stopDetection = function(){

	this.stopWatchingPosition();
}
GpsManager.prototype.stopWatchingPosition = function(){

	if(this.handlerID)
		window.navigator.geolocation.clearWatch(this.handlerID);
}