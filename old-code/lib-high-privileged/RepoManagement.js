function ChromeFile(uri){

	this.uri = uri;
}
ChromeFile.prototype.readContent = function(){
	//TODO: Add a try
	var req = new XMLHttpRequest();
	req.open("GET", this.uri, false);
	req.send(null);
	return req.responseText;
}

function RepositoryManager(){

	this.defaultDirs = {

		augmenters: "chrome://MoWA/content/lib/defaults/augmenters/",
		applications: "chrome://MoWA/content/lib/defaults/applications/",
		spacereps: "chrome://MoWA/content/lib/defaults/space-representations/"
	};
}
RepositoryManager.prototype.initialize = function(){}
RepositoryManager.prototype.getAugmentersFiles = function(){

	var files = new Array();
	this.addAugmentersDefaultFiles(files);
	//TODO: add installed files

	return files;
}
RepositoryManager.prototype.addAugmentersDefaultFiles = function(files){

	var orderedAugmenters = [
		"MobileWebAugmenter",
		"SingleContainerBasedAugmenter",
		"Html5VideoAugmenter",
		"HtmlObjectAugmenter",
		"PathAugmenter",
		"PoiAugmenter",
		"ListAugmenter",
		"TextAugmenter",
		"WalkingLinksAugmenter"
	];

	for (var i = 0; i < orderedAugmenters.length; i++) {
		files.push(new ChromeFile(
			this.defaultDirs["augmenters"] + orderedAugmenters[i] + ".js"
		));	
	}
}
RepositoryManager.prototype.getSpaceRepresentationsFiles = function(){

	var files = new Array();
	this.addSpaceRepsDefaultFiles(files);
	//TODO: add installed files

	return files;
}
RepositoryManager.prototype.addSpaceRepsDefaultFiles = function(files){

	var orderedAugmenters = [
		"Core",
		"AbstractLeafletMap",
		"LeafletCustomMap",
		"LeafletOutdoorMap"
	];

	for (var i = 0; i < orderedAugmenters.length; i++) {
		files.push(new ChromeFile(
			this.defaultDirs["spacereps"] + orderedAugmenters[i] + ".js"
		));	
	}
}
RepositoryManager.prototype.getSpaceRepresentationFile = function(filename, ext){

	var file;
	if(this.checkFileContent(dir + filename + ext))
		file = new ChromeFile(this.defaultDirs["spacereps"] + filename + ext);	

	return file;
}
RepositoryManager.prototype.checkFileContent = function(uri){

	var req = new XMLHttpRequest();
	req.open("GET", uri, false);
	req.send(null);

	return (req.responseText && req.responseText.length > 0)? true: false;
}
RepositoryManager.prototype.retrieveAppClasses = function(){

	return [
		{className:"MobileHypermediaApp", readableName:"Mobile Hypermedia"}, 
		{className:"ContextAwareApp", readableName:"Context Aware"}, 
		{className:"AugmentedRealityApp", readableName:"Augmented Reality"}
	];
}
RepositoryManager.prototype.getAppBuildersData = function(){

	return [
		{
			classname:"MobileHypermediaAppBuilder", 
			readableName:"Mobile Hypermedia", 
			fileExtension:".xhtml"
		}
	];
}
RepositoryManager.prototype.getDefaultDir = function(classname){ //TODO: ???

	for (var i = 0; i < this.defaultDirs.length; i++) {
		if(this.defaultDirs[i].key == classname)
			return this.defaultDirs[i].value;
	};

	return undefined;
}