

/**
 * Represents a script which contain its file name and a set of methods which depend on its state 
 * @constructor
 */
function Application(className, fileExt){

 	ScriptInstance.prototype.constructor.call();

 	this.setClassName(className); 
 	this.setFileExt(fileExt);
 	this.state = new EnabledApplication();
 	this.appInstance;
 }
 Application.prototype = new ScriptInstance();
/**
 * This method is responsible for enable the script, but delegates this behavior to its state. 
 * Thereby each state knows what to do: if it's disabled this method enables it, and if 
 * it is already enabled does nothing.
 * @method
 * @public 
 * @memberof Application
 */
Application.prototype.enable = function() {

 	this.state.enable(this);
}
/**
 * This method is responsible for disable the script, but delegates this behavior to its state. 
 * Thereby each state knows what to do: if it's enabled this method disables it, and if 
 * it is already disables does nothing.
 * @method
 * @public 
 * @memberof Application
 */
Application.prototype.disable = function() {

 	this.state.disable(this);
}
Application.prototype.run = function(poi, location, augManager) {

 	if(this.appInstance)
 		this.state.run(this.appInstance, poi, location, augManager);
}
Application.prototype.initialize = function(dir) {

	console.log('Initializing application');
	try{
		var src = new FilesManager().readContentFrom(dir, this.getClassName() + this.getFileExt());

		if(src){
			var ctx = new Components.utils.Sandbox(window,{
				'sandboxPrototype': window,
				'wantXrays': true
			});
			Components.utils.evalInSandbox(src, ctx);

			var className = new MetadataProcessor().getMetadataValue(src, "className");
			var classType = new MetadataProcessor().getMetadataValue(src, "classType");
			var instance = new ctx[className];

			if(instance){

				instance.setClassName(className);
				instance.setClassType(classType);

				var namespace = new MetadataProcessor().getMetadataValue(src, "namespace");
				if(typeof(namespace) == undefined)
					namespace = "default";

				instance.setNamespace(namespace);
				instance.setFileExt(this.fileExt);

				window[className] = className;
			}

			console.log('4 Initializing application');
			if(instance){
				this.setAppInstance(instance);
				this.getAppInstance().initialize();
				return true;
			}
			else{
				window.NativeWindow.toast.show("Error loading script. "+ 
				"No class name found in metadata.", "short"); //TODO: localize and personalize message
			}
		}
		else{ 
			window.NativeWindow.toast.show("No se ha podido leer el source code", "short"); //TODO:
		}
	}
	catch(err) { 
		window.NativeWindow.toast.show(
			MoWA.getLanguageBundle("ScrManMod").formatStringFromName( "errorInitializingScript", 
				[this.getClassName()], 1) + err.message, "short"); 
	}
	return false;
}
Application.prototype.getStateName = function(sourceCode) {

	return this.state.getStateName();
}
Application.prototype.setAppInstance = function(appInstance) {

	this.appInstance = appInstance;
}
Application.prototype.getAppInstance = function() {

	return this.appInstance;
}
Application.prototype.clearApplicationData = function() {

	if(this.appInstance)
		this.appInstance.clearData();
}

function ApplicationState(){
}
ApplicationState.prototype.enable = function(ctx) {

	return null;
}
ApplicationState.prototype.disable = function(ctx) {

	return null;
}
ApplicationState.prototype.run = function(appInstance, poi, retrievedLocation, augManager) {

	return null;
}
ApplicationState.prototype.getStateName = function() {

	//return this.constructor.name;
}

function EnabledApplication(){	
}
EnabledApplication.prototype = new ApplicationState();
EnabledApplication.prototype.disable = function(ctx) {
	ctx.state = new DisabledApplication();
}
EnabledApplication.prototype.run = function(appInstance, poi, retrievedLocation, augManager) {

	try{ 
		appInstance.load(poi, retrievedLocation, augManager); 
	}
	catch(err) { window.NativeWindow.toast.show(err.message, "short"); }
}
EnabledApplication.prototype.getStateName = function() {

	return "EnabledApplication";
}

function DisabledApplication(){
}
DisabledApplication.prototype = new ApplicationState();
DisabledApplication.prototype.enable = function(ctx) {
	ctx.state = new EnabledApplication();
}
DisabledApplication.prototype.getStateName = function() {

	return "DisabledApplication";
}

function OrderedApplication(script){
	this.next = null;
	this.script = script;
}
OrderedApplication.prototype.hasNext = function(){	
	//Returns a Boolean which indicates whether the current OrderedApplication has linked a following or not.
	
	return (this.next == null)? false: true;
}
OrderedApplication.prototype.setNext = function(next){	
	//Sets a next OrderedApplication.
	
	this.next = next;
}
OrderedApplication.prototype.getNext = function(){	
	//Gets the next OrderedApplication.
	
	return this.next;
}
OrderedApplication.prototype.enable = function() {
	
	this.script.enable(this.script);
}
OrderedApplication.prototype.disable = function() {
	
	this.script.disable(this.script);
}
OrderedApplication.prototype.initialize = function(dir) {
	return this.script.initialize(dir);
}
OrderedApplication.prototype.run = function(poi, retrievedLocation, augManager) {
	
	this.script.run(poi, retrievedLocation, augManager);
}
OrderedApplication.prototype.getStateName = function(sourceCode) {

	return this.script.getStateName();
}
OrderedApplication.prototype.setAppInstance = function(appInstance) {

	this.script.setAppInstance(appInstance);
}
OrderedApplication.prototype.getAppInstance = function() {

	return this.script.getAppInstance();
}
OrderedApplication.prototype.clearApplicationData = function() {

	this.script.clearApplicationData();
}
OrderedApplication.prototype.getClassName = function(){

	return this.script.getClassName();
}
OrderedApplication.prototype.setClassName = function(className){

	this.script.setClassName(className);
}
OrderedApplication.prototype.getNamespace = function(){

	return this.script.getNamespace();
}
OrderedApplication.prototype.setNamespace = function(namespace){

	this.script.setNamespace(namespace);
}
OrderedApplication.prototype.getFileExt = function(){

	return this.script.getFileExt();
}
OrderedApplication.prototype.setFileExt = function(fileExt){

	this.setFileExt(fileExt);
}

function ScriptInstancesEditor(manager){

	this.manager = manager;
}
ScriptInstancesEditor.prototype.showView = function(){	
	
	var editor = this;
	var linkApplicationingEditorView = function(){

		editor.loadScriptsAsMenuItems(editor.manager.getScripts());
		window.removeEventListener("DOMContentLoaded", linkApplicationingEditorView, false); 
	}

	window.addEventListener("DOMContentLoaded", linkApplicationingEditorView, false); 
	window.BrowserApp.loadURI("chrome://MoWA/content/data/scripting-editor.html");
}
ScriptInstancesEditor.prototype.loadScriptsAsMenuItems = function(scripts){
	
	var scriptsListDiv = window.content.document.getElementById("scripts-list");
	var editor = this;

	for (var i = 0; i < scripts.length; i++) {

		var scriptDiv = window.content.document.createElement("div");
		scriptDiv.className = "scriptItem";
		scriptDiv.id = scripts[i].getClassName();
		scriptDiv.appendChild(
			window.content.document.createTextNode(
				scripts[i].getClassName()
				));
		scriptDiv.scriptOrder = i;

		scriptDiv.addEventListener("click", function(){

			var contextMenu = editor.createContextMenu(scripts[this.scriptOrder]);
			window.content.document.body.appendChild(contextMenu);

		}, false);

		scriptsListDiv.appendChild(scriptDiv);
	};	
}
ScriptInstancesEditor.prototype.createContextMenu = function(script){}
ScriptInstancesEditor.prototype.createContextMenuItem = function(name, func){

	var menuItem = window.content.document.createElement("div");
	menuItem.className = "contextMenuItem";
	menuItem.appendChild(window.content.document.createTextNode(name));

	menuItem.addEventListener("click", func, false);

	return menuItem;
}

function ApplicationsEditor(manager){

	this.manager = manager;
}
ApplicationsEditor.prototype = new ScriptInstancesEditor();
ApplicationsEditor.prototype.createContextMenu = function(script){
	var contextMenu = window.content.document.createElement("div");
	contextMenu.className = "contextMenu";
	var self = this;

	var removeMenuItem = this.createContextMenuItem(
		MoWA.getLanguageBundle("ScrManMod").GetStringFromName("delete"),
		function(){		
			try{						
				self.manager.uninstallScript(script.getClassName() + script.getFileExt());
				window.content.document.body.removeChild(contextMenu);
				self.manager.showEditor();
				window.NativeWindow.toast.show(
					MoWA.getLanguageBundle("ScrManMod").GetStringFromName("scriptWasUninstalled"), 
					"short");

			}catch(err){window.NativeWindow.toast.show(err.message, "long")}
		}
		);

	var clearAppDataMenuItem = this.createContextMenuItem(
		MoWA.getLanguageBundle("ScrManMod").GetStringFromName("clearScriptData"),
		function(){		
			try{						
				self.manager.clearApplicationData([script]);
				window.content.document.body.removeChild(contextMenu);
				self.manager.showEditor();

			}catch(err){window.NativeWindow.toast.show(err.message, "long")}
		}
		);

	contextMenu.appendChild(removeMenuItem);
	contextMenu.appendChild(clearAppDataMenuItem);

	return contextMenu;
}

function AugmentersEditor(manager){

	this.manager = manager;
}
AugmentersEditor.prototype = new ScriptInstancesEditor();
AugmentersEditor.prototype.createContextMenu = function(script){

	var contextMenu = window.content.document.createElement("div");
	contextMenu.className = "contextMenu";
	var self = this;

	contextMenu.appendChild(
		this.createContextMenuItem(
			MoWA.getLanguageBundle("ScrManMod").GetStringFromName("delete"),
			function(){		
				try{						
					self.manager.uninstallScript(script.getClassName() + script.getFileExt());
					window.content.document.body.removeChild(contextMenu);
					self.manager.showEditor();
					window.NativeWindow.toast.show(
						MoWA.getLanguageBundle("ScrManMod").GetStringFromName("scriptWasUninstalled"), 
						"short");

				}catch(err){window.NativeWindow.toast.show(err.message, "long")}
			}
			));

	return contextMenu;
}

function ScriptInstancesManager(){

	this.notFoundClasses = new Array();
	this.instances = new Array();
	this.scripts = new Array();
	this.DEF_DIR_NAME;
	this.scriptExt = ".js";
	this.editor = undefined;
}
ScriptInstancesManager.prototype.setAugmentersEditor = function(){

	this.editor = new AugmentersEditor(this);
}
ScriptInstancesManager.prototype.setAppsEditor = function(){

	this.editor = new ApplicationsEditor(this);
}
ScriptInstancesManager.prototype.getDefDirName = function(){

	return this.DEF_DIR_NAME;
}
ScriptInstancesManager.prototype.getExtPointClassName = function(){

	return this.DEF_DIR_NAME;
}
ScriptInstancesManager.prototype.addNotFoundClass = function(className){

	this.notFoundClasses.push(className);
}
ScriptInstancesManager.prototype.removeNotFoundClass = function(className){

	for (var i = 0; i < this.notFoundClasses.length; i++) {
		if(this.notFoundClasses[i] == className){
			this.notFoundClasses.splice(i, 1);
			break;
		}
	};
}
ScriptInstancesManager.prototype.checkNotFoundClasses = function(className){ //Abstract
}
ScriptInstancesManager.prototype.isClassLoaded = function(className){ //Abstract
}
ScriptInstancesManager.prototype.loadScript = function(src){ 

	if(new MetadataProcessor().evaluateRules(src, this)){

		var instance = this.createInstance(src);
		if(instance){ this.addInstance(instance); }
		else {
			window.NativeWindow.toast.show(
				MoWA.getLanguageBundle("ScrManMod").formatStringFromName(
					"classCoudntBeInstantiated", 
					[new MetadataProcessor().getMetadataValue(src, "className")], 1
					),"long");
			return false;
		}

		return true;
	}
	return false;
}
ScriptInstancesManager.prototype.removeScriptFile = function(fullScriptName, dir){

	try{
		var filesManager = new FilesManager();
		if(filesManager.getFile(dir, fullScriptName) != undefined)
			filesManager.removeFile(fullScriptName, [dir]);		

	}catch(err){
		window.NativeWindow.toast.show(err.message, "long");
	}
}
ScriptInstancesManager.prototype.checkIfScriptExists = function(scriptName, dirName){

	return new FilesManager().fileExists(dirName, scriptName);
}
ScriptInstancesManager.prototype.writeScriptFile = function(scriptType, className, src, charset){

	return new FilesManager().writeFile(scriptType, className, src, charset);
}
ScriptInstancesManager.prototype.installDependencies = function(src, charset, term){

	var requiredClasses = new MetadataProcessor().getMetadataValue(src, term);

	if(requiredClasses){

		requiredClasses = requiredClasses.trim().split(" ");

		for (var i = 0; i < requiredClasses.length; i++) {
			if(!this.isClassLoaded(requiredClasses[i])){

				var depSrc = this.lookForScriptInRepo(
					this.getDefDirName(), 
					requiredClasses[i] + this.scriptExt
					);

				if(depSrc == "") {
					window.NativeWindow.toast.show(
						MoWA.getLanguageBundle("ScrManMod").formatStringFromName("notFoundClassInRepo", [requiredClasses[i]], 1),
						"long");
					return false;
				}
				
				if (!(this.installScript(depSrc, charset))){
					window.NativeWindow.toast.show(
						MoWA.getLanguageBundle("ScrManMod").formatStringFromName("dependencyCoudntBeInstalled", [requiredClasses[i]], 1),
						"long");
					return false;
				}
			}
		};
	}
	return true;
}
ScriptInstancesManager.prototype.lookForScriptInRepo = function(dir, fullFilename){ //TMP
	//TODO: replace internal search for repo search
	
	return new FilesManager().readContentFrom(dir, fullFilename);
}
ScriptInstancesManager.prototype.installScript = function(src){

	var mProcessor = new MetadataProcessor(),
	className = mProcessor.getMetadataValue(src, "className"),
	scriptType = mProcessor.getMetadataValue(src, "scriptType");

	if(	this.checkIfScriptExists(className + this.scriptExt, scriptType))
		this.removeScriptFile(className + this.scriptExt, scriptType);

	if(	this.installDependencies(src, this.charset , "requiredAugmenters") &&
		this.installDependencies(src, this.charset , "requiredApps") &&
		this.writeScriptFile(scriptType, className + this.scriptExt, src, this.charset ) &&
		this.loadScript(src)){

		window.NativeWindow.toast.show(
			MoWA.getLanguageBundle("ScrManMod").GetStringFromName("sucessfulScriptInstallation"), 
			"short"); 
		return true;
	}	
}
ScriptInstancesManager.prototype.uninstallScript = function(fullScriptName){	
	
	this.removeScriptFile(fullScriptName, this.getDefDirName());
	this.unloadScript(fullScriptName);
}
ScriptInstancesManager.prototype.unloadScript = function(fullFilename){ //Abstract	
}
ScriptInstancesManager.prototype.getScripts = function(){ //Abstract	
}
ScriptInstancesManager.prototype.showEditor = function(){	

	this.editor.showView();
}
ScriptInstancesManager.prototype.getScripts = function(){} //Abstract



function SpaceRepresentationsManager(charset, ctx){

	ScriptInstancesManager.prototype.constructor.call();
	this.initialize(charset, ctx);
}
SpaceRepresentationsManager.prototype = new ScriptInstancesManager();
SpaceRepresentationsManager.prototype.initialize = function(charset, ctx) {

	try{
		this.charset = charset || "ISO-8859-1";
		this.DEF_DIR_NAME = "SpaceRepresentation"; //TODO: check "DEF_DIR_NAME" usage. It was partially replaced with Repo manager but still used for new installed dirs
		this.instances = new Array();
		this.buildingCtx = ctx;
		this.loadExistingScripts();
	}catch(err){window.NativeWindow.toast.show(err.message, "short");}	
}
SpaceRepresentationsManager.prototype.loadExistingScripts = function() {

	var files = new RepositoryManager().getSpaceRepresentationsFiles();
	for (var i = 0; i < files.length; i++) {
		
		var src = files[i].readContent();
		if(src)
			this.loadScript(src);
	};
}
SpaceRepresentationsManager.prototype.createInstance = function (src) {

	Components.utils.evalInSandbox(src, this.buildingCtx);
	var name = new MetadataProcessor().getMetadataValue(src, "name");
	var className = new MetadataProcessor().getMetadataValue(src, "className");
	var classType = new MetadataProcessor().getMetadataValue(src, "classType");
	var params = new MetadataProcessor().getMetadataValues(src, "execParam"); 

	console.log('Initialazing space representation instance');
	var instance = new this.buildingCtx[className];
	if(instance){

		instance.setName(name);
		instance.setClassName(className);
		instance.setClassType(classType);
		instance.setNamespace(this.getNamespace(src));
		instance.setFileExt(this.scriptExt);
		if(params)
			instance.setParamsForConfiguration(params);
	}

	return instance;
}
SpaceRepresentationsManager.prototype.addInstance = function(instance) {
	
	if(this.getAugmenter(augInstance.className)){
		//TODO: add message the space rep already exists
		return;
	}
	this.instances.push(instance);
}


function AugmentersManager(charset, ctx){

	ScriptInstancesManager.prototype.constructor.call();
	this.charset; 

	this.initialize(charset, ctx);
}
AugmentersManager.prototype = new ScriptInstancesManager();
AugmentersManager.prototype.initialize = function(charset, ctx) {

	try{
		this.charset = charset || "ISO-8859-1";
		this.DEF_DIR_NAME = "MobileWebAugmenter";
		this.instances = new Array();
		this.setAugmentersEditor();
		this.buildingCtx = ctx;
		this.loadExistingScripts();
	}catch(err){window.NativeWindow.toast.show(err.message, "short");}	
}
AugmentersManager.prototype.getAugmenters = function(){

	return this.instances;
}
AugmentersManager.prototype.loadExistingScripts = function() {

	var files = new RepositoryManager().getAugmentersFiles();
	for (var i = 0; i < files.length; i++) {
		
		var src = files[i].readContent();
		if(src)
			this.loadScript(src);
	};
}
AugmentersManager.prototype.createInstance = function (src) {

	Components.utils.evalInSandbox(src, this.buildingCtx);
	var name = new MetadataProcessor().getMetadataValue(src, "name");
	var className = new MetadataProcessor().getMetadataValue(src, "className");
	var classType = new MetadataProcessor().getMetadataValue(src, "classType");
	var params = new MetadataProcessor().getMetadataValues(src, "execParam"); 

	var instance = new this.buildingCtx[className];

	console.log('Initializing augmenter instance');
	if(instance){

		instance.setName(name);
		instance.setClassName(className);
		instance.setClassType(classType);
		instance.setNamespace(this.getNamespace(src));
		instance.setFileExt(this.scriptExt);
		if(params)
			instance.setParamsForConfiguration(params);
	}

	return instance;
}
AugmentersManager.prototype.getNamespace = function(src){

	var namespace = new MetadataProcessor().getMetadataValue(src, "namespace");

	if(typeof(namespace) == undefined)
		return "default";
	
	return namespace; 
}
AugmentersManager.prototype.addInstance = function(instance) {
	
	if(this.getAugmenter(instance.className)){
		//TODO: add message the augmenter already exists
		return;
	}
	this.instances.push(instance);
}
AugmentersManager.prototype.getAugmenter = function(className){

	for (var i = 0; i < this.instances.length; i++) {
		if(this.instances[i].className == className)
			return this.instances[i];
	};
	return undefined;
}
AugmentersManager.prototype.checkNotFoundClasses = function(className){

	for (var i = 0; i < this.notFoundClasses.length; i++) {
		for (var j = 0; j < this.instances.length; j++) {
			
			if(this.notFoundClasses[i] == this.instances[j].name) 
			{
				this.removeNotFoundClass(notFoundClasses[i]);
				break;
			}

		}
	};

	if(this.notFoundClasses.length > 0){

		window.NativeWindow.toast.show("No se ha podido caragar las siguientes clases: " + this.notFoundClasses.toString(), "long"); //TODO: bundle
	}
}
AugmentersManager.prototype.isClassLoaded = function(className){

	return (this.getAugmenter(className))? true: false;
}
AugmentersManager.prototype.unloadScript = function(fullFilename){	

	for (var i=0; i<this.instances.length; i++){

		if(this.instances[i].getClassName() + this.instances[i].getFileExt() == fullFilename)
		{
			this.instances.splice(i,1);
			break;
		}
	}
}
AugmentersManager.prototype.getScripts = function(){	

	return this.getAugmenters();
}

function ApplicationsManager(charset, augManager){

	ScriptInstancesManager.prototype.constructor.call();

	this.charset;
	this.appsLauncher;
	this.orderedApplications;
	this.firstOrderedApplication;
	this.augmentersManager;

	this.initialize(charset, augManager);
}
ApplicationsManager.prototype = new ScriptInstancesManager();
ApplicationsManager.prototype.initialize = function(charset, augManager){

	try{
		this.charset = charset;
		this.DEF_DIR_NAME = "MobileApplication";
		this.orderedApplications = new Array();
		this.augmentersManager = augManager;

		var files = new FilesManager().readFilesFromDir(this.getDefDirName());
		var appScripts = this.getApplicationsFromFiles(files);

		this.loadWrappers(appScripts);
		this.appsLauncher = new ApplicationsLauncher();
		this.setAppsEditor();

		this.initializeApps(this.orderedApplications); //Se pasa por param porque no siempre van a ser todos los ordererd
	}catch(err){window.NativeWindow.toast.show(err.message, "short");}	
}
ApplicationsManager.prototype.getApplicationsFromFiles = function(files){

	var apps = new Array();

	for (var i = 0; i < files.length; i++) {
		apps.push(new Application(files[i].getName()));
	};

	return apps;
}
ApplicationsManager.prototype.getScripts = function(){	

	return this.getWrappedApps();
}
ApplicationsManager.prototype.getWrappedApps = function(){	

	return this.orderedApplications;
}
ApplicationsManager.prototype.loadScript = function(src){
	
	try{
		var mProcessor = new MetadataProcessor(),
		className = mProcessor.getMetadataValue(src, "className"),	
		scriptType = mProcessor.getMetadataValue(src, "scriptType"),
		app = new Application(className);

		this.loadWrappers([app]);
		this.initializeApps([app]);

		return true;

	}catch(err){}

	return false;
}
ApplicationsManager.prototype.loadWrappers = function(scripts){	
	//Creates the OrderedApplications from a set of ApplicationFiles, in the order in which they have been loaded.

	for(var i = 0; i<scripts.length; i++){
		this.addWrappedApp(new OrderedApplication(scripts[i]));
	}
}
ApplicationsManager.prototype.unloadScript = function(fullFilename){	

	for (var i=0; i<this.orderedApplications.length; i++){

		if(this.orderedApplications[i].getClassName() + this.orderedApplications[i].getFileExt() == fullFilename)
		{
			this.orderedApplications.splice(i,1);
			break;
		}
	}
}
ApplicationsManager.prototype.addWrappedApp = function(orderedApplication){	

	this.unloadScript(orderedApplication.getClassName() + orderedApplication.getFileExt());
	this.orderedApplications.push(orderedApplication);
}
ApplicationsManager.prototype.clearApplicationData = function(apps){	
	
	for (var i = 0; i < apps.length; i++) {
		apps[i].clearApplicationData();
	};

	window.NativeWindow.toast.show(
		MoWA.getLanguageBundle("ScrManMod").GetStringFromName("dataWasCleared"), 
		"short");
}
ApplicationsManager.prototype.getAppByClassName = function(className){	
	
	for (var i = 0; i < this.orderedApplications.length; i++) {
		if(this.orderedApplications[i].getClassName() == className)
			return this.orderedApplications[i];
	};

	return undefined;
}
ApplicationsManager.prototype.initializeApps = function(apps){

	this.appsLauncher.initializeApps(apps, this.getDefDirName())
}
ApplicationsManager.prototype.isClassLoaded = function(className){ //Abstract

	return (this.getAppByClassName(className) || this.augmentersManager.getAugmenter(className))? true: false;
}
ApplicationsManager.prototype.showAdaptation = function(retrievedData, retrievedLocation, augmentersManager){

	this.appsLauncher.showAdaptation(
		retrievedData, 
		retrievedLocation, 
		augmentersManager,
		this.orderedApplications
		);
}

/**
 * Represents a script launcher responsible for executing each enabled script.
 * @constructor
 */
function ApplicationsLauncher(){}
/**
 * Initializes each of the installed scripts
 * @method
 * @private
 * @memberof ApplicationsLauncher
 */
ApplicationsLauncher.prototype.initializeApps = function(apps, dir){	

 	for (var i = 0; i < apps.length; i++) {
 		apps[i].initialize(dir);
 	};
 }
/**
 * Executes each of the enabled scripts
 * @method
 * @private
 * @memberof ApplicationsLauncher
 */
ApplicationsLauncher.prototype.launchApplications = function(poi, retrievedLocation, orderedApps, augmentersManager){	

 	for (var i = 0; i < orderedApps.length; i++) {
 		orderedApps[i].run(poi, retrievedLocation, augmentersManager);
 	};
 }
ApplicationsLauncher.prototype.getApplyingPois = function(retrievedLocation, apps){

 	var pois = new Array;

 	for (var i = 0; i < apps.length; i++) {

 		var poi = apps[i].getAppInstance().poiManager.getPoiFromLocation(retrievedLocation);
 		if(poi != null)
 			pois.push(poi);
 	};

 	return pois;
 }	
 ApplicationsLauncher.prototype.onLoadWebPage = function(currentUrl){	
	//
	//
}
ApplicationsLauncher.prototype.showAdaptation = function(retrievedData, retrievedLocation, augmentersManager, orderedApps){

	var applyingPois = this.getApplyingPois(retrievedLocation, orderedApps);

	if (this.checkSingleUrlUse(applyingPois)){
		this.showSingleChoiceAdaptation(applyingPois[0], retrievedLocation, orderedApps, augmentersManager);
	}
	else if (applyingPois.length > 1){
		this.showAdaptationChoicesToUser(applyingPois[0], retrievedLocation, orderedApps, augmentersManager);
	}
	else{
		this.showRetrievedData(retrievedData);
	}
}
/**
 * This method indicates whether the retrieved data is a valid geo-positioning coordinate
 * @method
 * @access private 
 * @memberof ApplicationsLauncher
 * @returns {Boolean}
 */
ApplicationsLauncher.prototype.isValidCoordinate = function(retrievedData){

 	if (retrievedData.length!=2)
 		return false

 	var coordinates = retrievedData[0] + "," + retrievedData[1];
 	var coordinatesRegex = /^(\-?\d+(\.\d+)?),\s*(\-?\d+(\.\d+)?)$/;

 	return coordinatesRegex.test(coordinates);
 }
/**
 * This method check whether the adaptation urls of every PoI is used just for one of them
 * @method
 * @access private 
 * @memberof ApplicationsLauncher
 * @returns {Boolean}
 */
ApplicationsLauncher.prototype.checkSingleUrlUse = function(pois){
 	var urls = new Array;

 	for (var i = 0; i < pois.length; i++) {
 		urls.push(pois[i].getUrl());
 	};

 	var applyingUrls = urls.reduce(
 		function(previousValue, currentValue){
 			if(previousValue.indexOf(currentValue)<0)
 				previousValue.push(currentValue);

 			return previousValue;
 		},[]
 		);

 	return ((applyingUrls.length==1)? true: false);
 }
 ApplicationsLauncher.prototype.isUrl = function(retrievedData){

 	var regEx = "^(http|https)\://[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(:[a-zA-Z0-9]*)?/?([a-zA-Z0-9\-\._\?\,\'/\\\+&amp;%\$#\=~])*$";
 	var urlregex = new RegExp(regEx);

 	return urlregex.test(retrievedData);
	//return (retrievedData.indexOf("http://") === 0 || retrievedData.indexOf("https://") === 0)
}
ApplicationsLauncher.prototype.showAdaptationChoicesToUser = function(poi, retrievedLocation, orderedApps, augmentersManager){

	window.NativeWindow.toast.show("showAdaptationChoicesToUser", "short");
	this.showSingleChoiceAdaptation(poi, retrievedLocation, orderedApps, augmentersManager);

	/*
	var launcher = this;
	var linkOptionsView = function(){

		var itemsListDiv = window.content.document.getElementById("itemsList");
		var title = window.content.document.getElementById("title");
		title.appendChild(window.content.document.createTextNode(
			MoWA.getLanguageBundle("ScrManMod").GetStringFromName("locationWithMultiplePoi")
			));
		
		//A new option item is added to a list
		for (var i = 0; i < launcher.orderedApplications.length; i++) {

			var application = launcher.orderedApplications[i].getAppInstance();
			var poi = application.poiManager.getPoiFromLocation(retrievedLocation);
		
			if(poi != null){

				var listItem = window.content.document.createElement("div");
				listItem.className = "listItem";

					var itemName = window.content.document.createElement("p");
					itemName.className = "itemName";
					itemName.appendChild(window.content.document.createTextNode(poi.getName()));

					var itemAppName = window.content.document.createElement("p");
					itemAppName.className = "itemAppName";
					itemAppName.appendChild(
						window.content.document.createTextNode(
							MoWA.getLanguageBundle("ScrManMod").GetStringFromName("application") + ": " + 
							application.getName() 
						));

					var itemUrl = window.content.document.createElement("p");
					itemUrl.className = "itemUrl";
					itemUrl.appendChild(window.content.document.createTextNode("URL: " + poi.getUrl()));

				listItem.appendChild(itemName);
				listItem.appendChild(itemAppName);
				listItem.appendChild(itemUrl);
				listItem.poi = poi;

				listItem.onclick = function(){
					
					launcher.showSingleChoiceAdaptation(this.poi, retrievedLocation);
				}

				itemsListDiv.appendChild(listItem);
			}
		};

		window.removeEventListener("DOMContentLoaded", linkOptionsView, false); 
	}

	window.addEventListener("DOMContentLoaded", linkOptionsView, false); 
	window.BrowserApp.loadURI("chrome://MoWA/content/data/common-pois-chooser.html");
	*/
}
ApplicationsLauncher.prototype.showSingleChoiceAdaptation = function(poi, retrievedLocation, orderedApps, augmentersManager){

	//Function to be excecuted on document loading 
	var launcher = this;
	var launchingApplications = function(){

		window.removeEventListener("DOMContentLoaded", launchingApplications, false); 
		launcher.launchApplications(poi, retrievedLocation, orderedApps, augmentersManager);
		
	}

	//Add the DOMContentLoaded event listener to window
	window.addEventListener("DOMContentLoaded", launchingApplications, false);
	window.BrowserApp.loadURI(poi.getUrl());//, window.BrowserApp.LOAD_FLAGS_IS_REFRESH);
}
ApplicationsLauncher.prototype.showRetrievedData = function(retrievedData){

	var launcher = this;
	var launchRetrievedDataViewer = function(){

		var title = window.content.document.getElementById("title");
		title.innerHTML = "";
		title.appendChild(window.content.document.createTextNode("Datos Recuperados"));

		var retrievedDataDiv = window.content.document.getElementById("retrievedData");
		retrievedDataDiv.innerHTML = "";

		if (launcher.isUrl(retrievedData)){
			
			var anchor = window.content.document.createElement('a');
			anchor.setAttribute('href',retrievedData);
			anchor.innerHTML = retrievedData;

			retrievedDataDiv.appendChild(anchor);
		}
		else if (launcher.isValidCoordinate(retrievedData)){

			retrievedDataDiv.appendChild(window.content.document.createTextNode( "Latitud:" + retrievedData[0] + " - Longitud: " + retrievedData[1]));
			retrievedDataDiv.appendChild(launcher.getMap(retrievedData[0], retrievedData[1]));			
		}
		else retrievedDataDiv.appendChild(window.content.document.createTextNode(retrievedData));

		window.removeEventListener("DOMContentLoaded", launchRetrievedDataViewer, false);		
	}

	window.addEventListener("DOMContentLoaded", launchRetrievedDataViewer, false); 
	window.BrowserApp.loadURI("chrome://MoWA/content/data/retrieved-data-viewer.html");
}
ApplicationsLauncher.prototype.getMap = function(latitude, longitude){
	// Google maps API "-32.0000343,-58.0000343"
	// Alternatives have been moved to an individual project 
	// https://www.dropbox.com/sh/zhsyhebsygh74nw/RV4CxPiK8d

	var img = new window.Image();
	var size = Math.round(window.innerWidth * 0.9);

	img.src = "http://maps.googleapis.com/maps/api/staticmap?center=" + latitude + "," + longitude + "&zoom=13&size=" + size  + "x" + size + "&sensor=true";
	
	return img;
}

function ScriptingManager(){

	this.augmentersManager;
	this.charset = "ISO-8859-1"; //TODO: make the SM pass this value to both SW, and make a setter and getter for this value. Also make SM(charset)
	this.appsManager;
	this.scriptsExt = ".js";
	this.initialize();	
}
ScriptingManager.prototype.initialize = function(){

	var buildingCtx = new Components.utils.Sandbox(window,{
		'sandboxPrototype': window,
		'wantXrays': true
	});

	this.augmentersManager = new AugmentersManager(this.charset, buildingCtx);
	this.spaceRepsManager = new SpaceRepresentationsManager(this.charset, buildingCtx);
	this.appsManager = new ApplicationsManager(this.charset, this.augmentersManager);
}
ScriptingManager.prototype.getAugmentersManager = function(){
	return this.augmentersManager;
}
ScriptingManager.prototype.showAppsEditor = function(){	

	this.appsManager.showEditor();
}
ScriptingManager.prototype.showAugmentersEditor = function(){	

	this.augmentersManager.showEditor();
}
ScriptingManager.prototype.checkForInstallableExtension = function(url, extensions){	

	for (var i = 0; i < extensions.length; i++) {
		if(extensions[i] == this.getUrlExtension(url)){
			
			return this.hasMowaMetadataBlockDefinition(this.readCurrentWebpageTextContent());
		}
	};

	return false;
}
ScriptingManager.prototype.getUrlExtension = function(url){	

	return (/[.]/.exec(url))? /[^.]+$/.exec(url) : null;
}
ScriptingManager.prototype.hasMowaMetadataBlockDefinition = function(src){

	return new MetadataProcessor().checkMetadataBlockDefinition(src);
}
ScriptingManager.prototype.readCurrentWebpageTextContent = function(){

	return window.content.document.getElementsByTagName("pre")[0].textContent;
}
ScriptingManager.prototype.installScript = function(src, charset){	

	try{
		if (this.checkCodeWithoutErrors(src, charset) && this.hasMowaMetadataBlockDefinition(src)){

			var mProcessor = new MetadataProcessor(),
			className = mProcessor.getMetadataValue(src, "className"),
			scriptType = mProcessor.getMetadataValue(src, "scriptType");

			return this["install" + scriptType + "Script"](src);  		
		}
	}catch(err){
		window.NativeWindow.toast.show(err.message, "short");
	}

	window.NativeWindow.toast.show(
		MoWA.getLanguageBundle("ScrManMod").GetStringFromName("scriptInstallationFailed"), 
		"short");

	return false;
}
ScriptingManager.prototype.installMobileApplicationScript = function(src){

	return this.appsManager.installScript(src);
}
ScriptingManager.prototype.installMobileWebAugmenterScript = function(src){

	return this.augmentersManager.installScript(src);
}
ScriptingManager.prototype.getFileNameFromUrl = function(url){
	
	return url.replace(/^.*[\\\/]/, ''); //returns Filename.ext
}
ScriptingManager.prototype.checkCodeWithoutErrors = function(src, charset){

	try{
		return true;

		//TODO: repair the commented block
	/*

		Components.utils.import("resource://gre/modules/FileUtils.jsm");
		var file = FileUtils.getNsiFile("TmpD", ["MoWA-tmp-files"]);

		var filename = "tmp-script.js";
		var directory = file.path;
   		var scriptContent = window.content.document.getElementsByTagName("pre")[0].innerHTML;

   		file.append(filename);

  		new FilesManager.writeFile(directory, filename, scriptContent);

		Components.utils.import('resource://gre/modules/Services.jsm' );
		Services.scriptloader.loadSubScript("file://" + file.path, this, charset); 

		//var app = this.getAppInstance();

		return true;*/
	}
	catch(err) { 
		window.NativeWindow.toast.show(
			MoWA.getLanguageBundle("ScrManMod").GetStringFromName("scriptContentError") + err.message, 
			"long");
	}
	return false;
}
/**
 * Verifies if the recovered QR location is associated with some point of interest
 * @method
 * @public
 * @memberof ScriptingManager
 */
 ScriptingManager.prototype.updateQRLocation = function(retrievedData){

 	this.updateLocation(new QRLocation(retrievedData), retrievedData);
 }
/**
 * Verifies if the recovered GPS location is associated with some point of interest
 * @method
 * @public
 * @memberof ScriptingManager
 */
 ScriptingManager.prototype.updateGpsLocation = function(retrievedData){

 	this.updateLocation(new GpsLocation(retrievedData[0], retrievedData[1]), retrievedData);
 }
 ScriptingManager.prototype.updateLocation = function(retrievedLocation, retrievedData){

	//TOCHECK: is retrievedData really neccesary??
	this.appsManager.showAdaptation(
		retrievedData, 
		retrievedLocation,
		this.augmentersManager
		);
}
