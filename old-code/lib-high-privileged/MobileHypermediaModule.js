function MobileHypermediaApp(name, traversingStrategy) {

	this.traversingStrategy;
	this.name; 
	
	this.setName(name);
	this.setTraversingStrategy(traversingStrategy);

	this.firstMobileHypermediaPois = new Array();
	this.mobileHypermediaPois = new Array();
	this.mobileHypermediaUser = new MobileHypermediaUser(this.user);
	this.defaultXpath;
	this.augManager;
};
MobileHypermediaApp.prototype = new MobileApplication();
MobileHypermediaApp.prototype.setTraversingStrategy = function(classname){

	try{
		eval("this.traversingStrategy = new " + classname + "();");

	}catch(err){
		this.setDefaultTraversingStrategy();
	}
}
MobileHypermediaApp.prototype.setDefaultTraversingStrategy = function(){

	this.traversingStrategy = new UnorderedStrategy();
}
MobileHypermediaApp.prototype.setDefaultXpath = function(xpath){

	this.defaultXpath = xpath;
}
MobileHypermediaApp.prototype.getDefaultXpath = function(){

	return this.defaultXpath;
}
MobileHypermediaApp.prototype.load = function(poi, sensedLocation, augManager){
	// This method adapts the web according to the loaded url

	this.mobileHypermediaUser.setCurrentLocation(sensedLocation);
	//Recentely commented this.setAugmentersManager(augManager);

	var currentUrl = poi.getUrl();

	if(!this.isValidUrlForAdaptation(currentUrl)) 
		return this.showUnknownPoiMessage();

	if(this.traversingStrategy.isPathCompleted(this.firstMobileHypermediaPois))
		return this.showFinalMessage();

	if(this.isTargetDefined()) 
	{
		if (!this.isValidUrlForTarget(currentUrl))
			return this.showWrongPoiMessage();
	}
	else 
	{
		if (this.traversingStrategy.isStrictMode() && 
			!this.traversingStrategy.isAStartingPoint(
				currentUrl, 
				this.firstMobileHypermediaPois
			)
		){
				return this.showStartingPointsMessage();
		}
	}

	var self = this;
	var callback = function (){
			self.setLocationAsVisited(self.mobileHypermediaUser.getCurrentLocation());
			self.loadAdaptation(currentUrl);
	}

	this.getReady(callback);
};
MobileHypermediaApp.prototype.loadAdaptation = function(url){
	// This method calls the appropriate adaptation method according to the current poi

	var adaptationFunctionAsString = this.getFunctionName(url);

	if (adaptationFunctionAsString != null)
	{
		this.loadTargetPoi();
		eval("this.adapt" + adaptationFunctionAsString + ";"); 
	}
};
MobileHypermediaApp.prototype.getFunctionName = function(url){ 
	//This method gets the name of the appropriate method to be called, 
	//according to the URL of the current page accessed
	
	var mobileHypermediaPois = this.firstMobileHypermediaPois.slice(0);

	while(mobileHypermediaPois.length != 0)
	{
		if(mobileHypermediaPois[0].getUrl() == url) 
		{
			var functionName = mobileHypermediaPois[0].poi.getName();
		
			if(functionName != null && functionName.replace(' ','') != '') 
				functionName = functionName.split(' ').join('') + "()";
			else 
			{
				functionName = null;
				window.NativeWindow.toast.show(
					MoWA.getLanguageBundle("MobHypMod").GetStringFromName("noPoiNameDefinedError"), 
					"short");
			}
			
			return  functionName;
		}
		else
		{
			mobileHypermediaPois = mobileHypermediaPois.concat(mobileHypermediaPois[0].getWalkingLinks());
			mobileHypermediaPois.shift();
		}
	}

	return null;
};
MobileHypermediaApp.prototype.showBaseMessage = function(params){

	// params = {message, elemId, xpathToPrevElem}

	//Se crea un p�rrafo con el t�tulo de la aplicaci�n
	var titleParagraph = window.content.document.createElement("p");
	titleParagraph.style.fontWeight = 'bold';
	titleParagraph.style.margin = "0";
	titleParagraph.appendChild(window.content.document.createTextNode(this.getName()));

	var id = (params.elemId == null || params.elemId == undefined)? this.getName() + "Base" : params.elemId;
	var xpath = (params.xpathToPrevElem == null || params.xpathToPrevElem == undefined)? this.getDefaultXpath() : params.xpathToPrevElem;
	
	//Se muestra el p�rrafo mediante un augmenter
	var htmlObjectAugmenter = this.getAugmenterInstance("HtmlObjectAugmenter").execute({
		"elemId": id, 
		"xpathToPrevElem": xpath, 
		"htmlObjects":[titleParagraph]
	});

	if(!(params.message == null || params.message == undefined)){

		this.getAugmenterInstance("TextAugmenter").execute({
			"elemId": id + "WarningMessage", 
			"xpathToPrevElem":"//*[@id='" + id + "']", 
			"text":params.message
		});
	}
};
MobileHypermediaApp.prototype.showFinalMessage = function(){
	// Hook method. The tour completion message is displayed

	this.showBaseMessage({
		"message": MoWA.getLanguageBundle("MobHypMod").GetStringFromName("endOfTourHasBeenReached")
	});
};
MobileHypermediaApp.prototype.showWrongPoiMessage = function(){
	// Hook method. A wrong way message is displayed.

	var baseId = this.getName() + "Base";
	
	this.showBaseMessage({
		"message": MoWA.getLanguageBundle("MobHypMod").GetStringFromName("wrongPoiMessage"),
		"elemId": baseId
	});

	this.getAugmenterInstance("PathAugmenter").execute({
		"elemId": this.getName()+"Path", 
		"xpathToPrevElem":"//*[@id='" + baseId + "']",
		"currentPoi": this.getCurrentPoi(),
		"targetPois": [this.getTargetPoi()]
	});
};
MobileHypermediaApp.prototype.showUnknownPoiMessage = function(){
	// Hook method. An unknown poi message is displayed.
	
	this.showBaseMessage({
		"message": MoWA.getLanguageBundle("MobHypMod").GetStringFromName("unknownPoiMessage")
	});
};
MobileHypermediaApp.prototype.showStartingPointsMessage = function(){
	// Hook method. A message is displayed when the user must move towards a starting point.

	var baseId = this.getName() + "Base";

	this.showBaseMessage({
		"message": MoWA.getLanguageBundle("MobHypMod").GetStringFromName("goToStartingPointMessage"),
		"elemId": baseId
	});

	var targetPois = new Array();

	for (var i = 0; i < this.firstMobileHypermediaPois.length; i++) {

		targetPois.push(this.firstMobileHypermediaPois[i]);
	};

	this.getAugmenterInstance("PathAugmenter").execute({
		"elemId": this.getName()+"Path", 
		"xpathToPrevElem":"//*[@id='" + baseId + "']",
		"currentPoi": this.getCurrentPoi(),
		"targetPois": targetPois
	});
};
MobileHypermediaApp.prototype.getPossibleNextPois = function(){
	// This method returns a collection of Pois which can be visited below.

	if(this.getCurrentPoi() == null)
		return null;

	var currentMobileHypermediaPoi = this.getMobileHypermediaPoiFromPoi(this.getCurrentPoi(), this.firstMobileHypermediaPois);
	
	return this.traversingStrategy.getPossibleNextMobileHypermediaPois(currentMobileHypermediaPoi);
};
MobileHypermediaApp.prototype.getNextWalkingLinks = function(){
	// This method returns a collection of Pois which can be visited below.

	if(this.getCurrentPoi() == null)
		return null;

	var currentMobileHypermediaPoi = this.getMobileHypermediaPoiFromPoi(this.getCurrentPoi(), this.firstMobileHypermediaPois);
	
	return currentMobileHypermediaPoi.getWalkingLinks();
};
MobileHypermediaApp.prototype.getMobileHypermediaPoiFromPoi = function(poi){
	// This method receives a Poi and searches within the 'firstMobileHypermediaPois' collection 
	// a MobileHypermediaPoi containing the same Poi received as parameter

	var mobileHypermediaPois = this.firstMobileHypermediaPois.slice(0);

	while(mobileHypermediaPois.length != 0){

		if(mobileHypermediaPois[0].getPoi().equalsTo(poi)) 
			return mobileHypermediaPois[0];
		else
		{
			mobileHypermediaPois = mobileHypermediaPois.concat(mobileHypermediaPois[0].getWalkingLinks());
			mobileHypermediaPois.shift();
		}
	}
};
MobileHypermediaApp.prototype.isValidUrlForTarget = function(currentUrl){
	// This method checks if current URL is equivalent to the target poi URL

	if (this.isTargetDefined())	
		return (this.getTargetPoi().getUrl() == currentUrl);

	else 
	{
		if (!this.traversingStrategy.isStrictMode())
			return true;

		for (var i = 0; i < this.firstMobileHypermediaPois.length; i++) {
			if (this.firstMobileHypermediaPois[i].getUrl() == currentUrl)
				return true;
		};
		
		return false;
	}
};
MobileHypermediaApp.prototype.isValidUrlForAdaptation = function(currentUrl){
	// This method checks if current url belongs to any of the pois of the application

	var mobileHypermediaPois = this.firstMobileHypermediaPois.slice(0);

	while(mobileHypermediaPois.length != 0){

		if(mobileHypermediaPois[0].getPoi().getUrl() == currentUrl) 
			return true;
		else
		{
			mobileHypermediaPois = mobileHypermediaPois.concat(mobileHypermediaPois[0].getWalkingLinks());
			mobileHypermediaPois.shift();
		}
	}

	return false;
};
MobileHypermediaApp.prototype.isTargetDefined = function(){
	// This method checks if the application has defined a target poi

	if(this.getTargetPoi() != null)
		return true

	return false;
};
MobileHypermediaApp.prototype.loadTargetPoi = function(){
	// This method loads the target poi in the MobileHypermediaUser

	var storageManager = new StorageManager();
	var targetURL = storageManager.restoreValue("targetUrl");
	
	if(targetURL)
		this.mobileHypermediaUser.setTargetMobileHypermediaPoi(
			this.getMobileHypermediaPoiFromPoi(
				this.poiManager.getPoiFromURL(targetURL)
			)
		);
	else this.mobileHypermediaUser.setTargetMobileHypermediaPoi(null);
};
MobileHypermediaApp.prototype.setTargetPoiFromURL = function(url){
	// This method sets the target poi looking at the application pois collection which one 
	// matches with the url received as parameter

	var storageManager = new StorageManager();
	storageManager.saveValue("targetUrl",url);

	this.loadTargetPoi();
};
MobileHypermediaApp.prototype.getTargetPoi = function(){
	this.loadTargetPoi();

	if(this.mobileHypermediaUser.targetMobileHypermediaPoi != null)
		return this.mobileHypermediaUser.getTargetMobileHypermediaPoi().getPoi();
	else return null;
};
MobileHypermediaApp.prototype.clearTargetPoi = function(){
	// This method clear the "targetUrl" variable in localstorage

	var storageManager = new StorageManager();
	storageManager.clearValue("targetUrl");
};
MobileHypermediaApp.prototype.savePathAsCompleted = function(){
	
	var storageManager = new StorageManager();
	storageManager.saveValue("completedPath","true");
};
MobileHypermediaApp.prototype.clearCompletedPath = function(){
	// This method clear the "completedPath" variable in localstorage

	var storageManager = new StorageManager();
	storageManager.clearValue("completedPath");
};
MobileHypermediaApp.prototype.clearData = function(){
	// This method clear the "targetUrl" variable in localstorage

	//Call to base
	MobileApplication.prototype.clearData.call(this /*, args...*/);

	//Clear Hypermedia objects
	this.clearTargetPoi();
	this.clearCompletedPath();
};
MobileHypermediaApp.prototype.addAsStartingPoi = function(mobileHypermediaPois){

	for (var i = 0; i < mobileHypermediaPois.length; i++) {
		this.firstMobileHypermediaPois.push(mobileHypermediaPois[i]);
	};
};
MobileHypermediaApp.prototype.createLeafPoi = function(name, url, location, spaceRep, extraProps, externalProps){

	var newPoi = new LeafPoi(name, url, location, spaceRep, extraProps, externalProps);
	this.poiManager.addPoiToCollection(newPoi);

	var newMobileHypermediaPoi = new MobileHypermediaPoi(newPoi);
	this.mobileHypermediaPois.push(newMobileHypermediaPoi);

	return newMobileHypermediaPoi;
};
MobileHypermediaApp.prototype.createCompositePoi = function(name, url, location, spaceRep, extraProps, externalProps, containedPois){

	var newPoi = new CompositePoi(name, url, location, spaceRep, containedPois, extraProps, externalProps);
	this.poiManager.addPoiToCollection(newPoi);

	var newMobileHypermediaPoi = new MobileHypermediaPoi(newPoi);
	this.mobileHypermediaPois.push(newMobileHypermediaPoi);

	return newMobileHypermediaPoi;
};
MobileHypermediaApp.prototype.createWalkingLink = function(previousMobileHypermediaPois, nextMobileHypermediaPois){
	
	for (var i = 0; i < previousMobileHypermediaPois.length; i++) {

		for (var j = 0; j < nextMobileHypermediaPois.length; j++) {
			previousMobileHypermediaPois[i].addWalkingLink(nextMobileHypermediaPois[j]);
		};	
	};
};

function TraversingStrategy(){};
TraversingStrategy.prototype.isStrictMode = function(){
	//Abstract: must return a Boolean
};
TraversingStrategy.prototype.isPathCompleted = function(firstMobileHypermediaPois){
	
	var storageManager = new StorageManager();
	var completedPath = storageManager.restoreValue("completedPath");

	if(completedPath)
		if(completedPath == "true")
			return true
	
	return false;
};
TraversingStrategy.prototype.isAStartingPoint = function(url, firstMobileHypermediaPois){
	
	return false;
};
TraversingStrategy.prototype.getPossibleNextMobileHypermediaPois = function(currentMobileHypermediaPoi){	
	//Abstract method. Devuelve una colecci�n con los MobileHypermediaPois que pueden ser navegados a continuaci�n.
};


function OrderedStrategy(){
	// Concrete class. It represents a navigation strategy that allows users visiting pois sequentially. 
	// This strategy suggests but doesn't impose a strict linear navigation, so some pois can be omitted.
	TraversingStrategy.prototype.constructor.call(this);
};
OrderedStrategy.prototype = new TraversingStrategy();
OrderedStrategy.prototype.isStrictMode = function(){
	//Abstract: must return a Boolean
	return false;
};
OrderedStrategy.prototype.isAStartingPoint = function(url, firstMobileHypermediaPois){
	
	for (var i = 0; i < firstMobileHypermediaPois.length; i++) {
		if (firstMobileHypermediaPois[i].getUrl() == url)
			return true;
	};

	return false;
};
OrderedStrategy.prototype.getPossibleNextMobileHypermediaPois = function(currentMobileHypermediaPoi){	
	//Concrete method. It returns a collection of MobileHypermediaPois that can be navigated below.

	return currentMobileHypermediaPoi.getWalkingLinks();
};


function StrictOrderedStrategy(){
	// Concrete class. It represents a navigation strategy that allows users visiting pois 
	// in a strictly sequential order. This means no pois can be omitted.
	TraversingStrategy.prototype.constructor.call(this);
};
StrictOrderedStrategy.prototype = new OrderedStrategy();
StrictOrderedStrategy.prototype.isStrictMode = function(){
	//Abstract: must return a Boolean
	return true;
};

function UnorderedStrategy(){	
	TraversingStrategy.prototype.constructor.call(this);
};
UnorderedStrategy.prototype = new TraversingStrategy();
UnorderedStrategy.prototype.isStrictMode = function(){
	//This method always returns false, because UnorderedStrategy
	//doesn't require a particular path order. 
	return false;
};
UnorderedStrategy.prototype.isAStartingPoint = function(url, firstMobileHypermediaPois){
	// In UnorderedStrategy a user can start his tour from any poi, so any poi is considered as a starting point.

	return true;
};
UnorderedStrategy.prototype.getPossibleNextMobileHypermediaPois = function(poi){
	//This method returns the following node to the current
	//In UnorderedNavigation strategy any node can be predecessor 
	//of the current node, except himself.
  
	/*var nodesExceptCurrent = new Array();
	
	for (var i=0; i<this.nodes.length; i++)
		if (this.nodes[i].getPoi().getUrl() != poi.getUrl())
			nodesExceptCurrent.push(this.nodes[i]);

	return nodesExceptCurrent;*/

	return null;
};

/**
 * Represents a wrapper for users of MoWA MobileHypermediaApps
 * @constructor
 * @param {User} user - The user to be wrapped. 
 */
function MobileHypermediaUser(user){
	User.prototype.constructor.call(this);

	this.user = user;
	this.targetMobileHypermediaPoi;
};
MobileHypermediaUser.prototype.getTargetMobileHypermediaPoi = function(){
	
	return this.targetMobileHypermediaPoi;
};
MobileHypermediaUser.prototype.setTargetMobileHypermediaPoi = function(mobileHypermediaPoi){
	
	this.targetMobileHypermediaPoi = mobileHypermediaPoi;
};
MobileHypermediaUser.prototype.setCurrentLocation = function(location){

	this.user.setCurrentLocation(location);
};
MobileHypermediaUser.prototype.getCurrentLocation = function(){

	return this.user.getCurrentLocation();
};
MobileHypermediaUser.prototype.getLocationsHistory = function(){

	return this.user.getLocationsHistory();
};

/**
 * Represents a wrapper for PoIs of MoWA MobileHypermediaApps
 * @constructor
 * @param {PointOfInterest} poi - The PoI to be wrapped. 
 */
function MobileHypermediaPoi(poi){

	this.poi = poi;
	this.walkingLinks = new Array();
};
MobileHypermediaPoi.prototype.getPoi = function(){
	
	return this.poi;
};
MobileHypermediaPoi.prototype.addWalkingLink = function(mobileHypermediaPoi){
	
	this.walkingLinks.push(mobileHypermediaPoi);
};
MobileHypermediaPoi.prototype.getWalkingLinks = function(){
	
	return this.walkingLinks;
};
MobileHypermediaPoi.prototype.getName = function(){

	return this.poi.getName();
};
MobileHypermediaPoi.prototype.getUrl = function(){

	return this.poi.getUrl();
};
MobileHypermediaPoi.prototype.createProperty = function(key, value){

	this.poi.createProperty(key, value);
};
MobileHypermediaPoi.prototype.createPropertyFrom = function(key, url, xpathExpression){
	
	this.poi.createPropertyFrom(key, url, xpathExpression);
};
MobileHypermediaPoi.prototype.increasePropertiesToBeRetrieved = function(){

	this.poi.increasePropertiesToBeRetrieved();
};
MobileHypermediaPoi.prototype.decreasePropertiesToBeRetrieved = function(){
	
	this.poi.decreasePropertiesToBeRetrieved();
};
MobileHypermediaPoi.prototype.setProperty = function(key, value){
	
	this.poi.setProperty(key, value);
};
MobileHypermediaPoi.prototype.propertyExists = function(key){
	
	return this.poi.propertyExists(key);
};
MobileHypermediaPoi.prototype.getProperty = function(key){
	
	return this.poi.getProperty(key);
};
MobileHypermediaPoi.prototype.addContainedPoi = function(poi){
	//Abstract in decorated class
	this.poi.addContainedPoi(poi);
};
MobileHypermediaPoi.prototype.getContainedPois = function(){
	//Abstract in decorated class
	return this.poi.getContainedPois();
};
MobileHypermediaPoi.prototype.getPhysicalLocation = function(){
	
	return this.poi.getPhysicalLocation();
};
MobileHypermediaPoi.prototype.getSpaceRepresentation = function(){
	
	return this.poi.getSpaceRepresentation();
};
MobileHypermediaPoi.prototype.setSpaceRepresentation = function(spaceRep){
	
	this.poi.setSpaceRepresentation(spaceRep);
};
MobileHypermediaPoi.prototype.isLoaded = function(){
	
	return this.poi.isLoaded();
};
MobileHypermediaPoi.prototype.getMarker = function(){
	
	return this.poi.getMarker();
};
MobileHypermediaPoi.prototype.getReady = function(callbackOnComplete){

	this.poi.getReady(callbackOnComplete);
};
MobileHypermediaPoi.prototype.setParentPoi = function(parentPoi){
	this.poi.setParentPoi(parentPoi);
};
MobileHypermediaPoi.prototype.equalsTo = function(poi){

	return this.poi.equalsTo(poi);
};